<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    protected $fillable = ['name','moto','logo', 'theme'];

      public function candidates()
    {
        return $this->hasMany('App\Models\Candidate', 'party_id', 'id');
    }
}
