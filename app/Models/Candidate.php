<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Candidate extends Model
{
     use Sluggable;
    protected $fillable = ['full_name', 'short_bio',
        'gender', 'municipality', 'number', 'party_id', 'ckaban',
        'slug', 'facebook', 'instagram', 'twitter', 'email', 'pcimage', 'phoneimage', 'votes'
 	];

    public function party()
    {
        return $this->belongsTo('App\Models\Party', 'party_id', 'id');
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'full_name'
            ]
        ];
    }
}
