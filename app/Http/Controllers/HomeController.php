<?php

namespace App\Http\Controllers;

use App\Models\Party;
use App\Services\CandidateService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( CandidateService $candidateService){

        $this->candidateService = $candidateService;

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         $array = $this->candidateService->getmunicipalities();
         list($array1, $array2) = array_chunk($array, ceil(count($array) / 2));
        return view('home', compact(['array1', 'array2']));
    }

    public function contactUs(Request $request)
    {

    }

}
