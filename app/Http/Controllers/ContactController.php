<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class   ContactController extends Controller
{
    public function index()
    {
        return view('contact');
    }

    public function sendContact(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'message' => 'required'
        ],
            [
            'name.required' => 'Emri kërkohet!',
            'email.required' => 'E-maili kërkohet',
            'message.required' => 'Mesazhi kërkohet'
        ]);
         $data =[
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'message' => $request->get('message'),
         ];
        try {

          Mail::to('info@ckabon.net')->send(new Contact($data));
         } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Diçka shkoi gabimisht. Per mesazh ju lutem dergoni email tek: info@tagdigitals.com');
        }
        return redirect()->back()->with('status', 'Mesazhi u dergua me sukses');
    }
}
