<?php

namespace App\Http\Controllers;

use App\Services\CandidateService;
use App\Models\Party;
use App\Models\Candidate;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    protected $candidateService;

    public function __construct(CandidateService $candidateService)
    {
        $this->candidateService = $candidateService;
    }

    public function index()
    {
        $candidates = Candidate::orderBy('full_name')->paginate(16);
        return view('candidate.index', compact('candidates'));
    }

    public function showPartyCandidates($id)
    {
        $party = Party::findOrFail($id);
        $candidates = Candidate::whereHas('party', function ($query) use ($party) {
                $query->where('acronym', $party->acronym);
            })->orderByRaw('CONVERT(number, SIGNED) asc')->paginate(16);
        return view('kandidatet', compact(['party', 'candidates']));
    }

    public function show($slug)
    {
        $candidate = $this->candidateService->findCandidateBySlug($slug);
        return view('candidate.show', compact('candidate'));
    }

    public function getAllAutocomplete($query)
    {
        $candidates = Candidate::take(10)->where('full_name', 'like', '%' . $query . '%')
            ->orWhere('municipality', 'like', '%' . $query . '%')
            ->orWhereHas('party', function ($myquery) use ($query) {
                $myquery->where('acronym', 'like', '%' . $query . '%');
            })
            ->orWhereHas('party', function ($myquery) use ($query) {
                $myquery->where('name', 'like', '%' . $query . '%');
            })
            ->get();

        $data = "";
        if ($candidates->count() > 0) {
            foreach ($candidates as $candidate) {
                $data .= '<a class="dropdown-item" href="/' . $candidate->slug . '">' . $candidate->full_name . ' ' . $candidate->party->acronym . ' ' . ucfirst($candidate->municipality) . '</a>';
            }
        }
        return response()->json([$data, 'count' => $candidates->count()]);
    }

    public function addVote($slug)
    {
        $candidate = $this->candidateService->findCandidateBySlug($slug);
        $candidate->increment('votes');
        return redirect()->back();
    }

}
