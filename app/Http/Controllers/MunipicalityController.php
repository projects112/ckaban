<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\Party;
use App\Services\CandidateService;

class MunipicalityController extends Controller
{
     protected $candidateService;

    public function __construct( CandidateService $candidateService){
      	$this->candidateService = $candidateService;
    }
    public function index()
    {
        $municipalities = $this->candidateService->getmunicipalities();
        $candidates = Candidate::all();
        return view('municipality.index', compact(['municipalities', 'candidates']));
    }

    public function show($municipality)
    {
        $municipalities = $this->candidateService->getmunicipalities();
        try {
        abort_if(!in_array($municipality, $municipalities), 404, 'Not Found' );
        $candidates = Candidate::where('municipality', $municipality)->get();
        }catch (\Exception $e) {
            abort(404);
        }
        return view('municipality.show', compact(['candidates', 'municipality']));

    }
     public function showMunicipalityParties($municipality)
    {
        $parties = Party::orderBy('acronym')->get();
        return view('municipality.party', compact(['municipality', 'parties']));
    }
    public function showAllParties()
    {
        $parties = Party::orderBy('acronym')->get();
        return view('municipality.allParties', compact('parties'));
    }
    public function showMunicipalityCandidates($municipality, $id)
    {
        $municipalities = $this->candidateService->getmunicipalities();
        $party = Party::findOrFail($id);
        try {
        abort_if(!in_array($municipality, $municipalities), 404, 'Not Found' );
        $candidates = Candidate::whereHas('party', function ($query) use ($party) {
                $query->where('acronym', $party->acronym);
            })
            ->where('municipality', $municipality)
            ->orderByRaw('CONVERT(number, SIGNED) asc')
            ->paginate(16);
        }catch (\Exception $e) {
            abort(404);
        }
        return view('municipality.showMunicipalityCandidates', compact(['candidates', 'municipality', 'party']));
    }

        //search candidates by query when having party, and municipality
    public function searchMunicipalityCandidates($komuna, $party, $query)
    {
            $candidates = Candidate::take(10)
            ->where('municipality', $komuna)
            ->where('full_name', 'like', '%' . $query . '%')
            ->whereHas('party', function ($myquery) use ($party) {
                $myquery->where('acronym', $party);
            })
            ->get();

        $data = "";
        if ($candidates->count() > 0) {
            foreach ($candidates as $candidate) {
                $data .= '<a class="dropdown-item" href="/' . $candidate->slug . '">' . $candidate->full_name . ' ' . $candidate->party->acronym . ' ' . ucfirst($candidate->municipality) . '</a>';
            }
        }
        return response()->json([$data, 'count' => $candidates->count()]);
    }

    //search candidates by query when having party only
    public function searchPartyCandidates($party, $query)
    {
            $candidates = Candidate::where('full_name', 'like', '%' . $query . '%')
            ->whereHas('party', function ($myquery) use ($party) {
                $myquery->where('acronym', $party);
            })
            ->get();
        $data = "";
        if ($candidates->count() > 0) {
            foreach ($candidates as $candidate) {
                $data .= '<a class="dropdown-item" href="/' . $candidate->slug . '">' . $candidate->full_name . ' ' . $candidate->party->acronym . ' ' . ucfirst($candidate->municipality) . '</a>';
            }
        }
        return response()->json([$data, 'count' => $candidates->count()]);
    }
}
