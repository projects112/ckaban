<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CandidateStoreRequest;
use App\Models\Candidate;
use App\Models\Party;
use App\Services\CandidateService;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Arr;

//use Intervention\Image\Image;

class CandidateController extends Controller
{
    protected $candidateService;

    public function __construct( CandidateService $candidateService){
      	$this->candidateService = $candidateService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates= Candidate::all();
        return view('admin.candidate.index', compact('candidates'));
    }
    public function showPartyCandidates($id){
        dd($id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parties = Party::all();
        $municipalities = $this->candidateService->getmunicipalities();
        sort($municipalities);
        return view('admin.candidate.create', compact(['parties', 'municipalities']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CandidateStoreRequest $request)
    {

        $candidate = new Candidate;
        $fillableFields = $candidate->getFillable();
        $filteredFields = $this->filterUnwanted(['pcimg', 'phoneimg', 'party_id'], $fillableFields);
        foreach($filteredFields as $field => $value){
            $this->declareIfIsset($value, $request, $candidate);
        }
        //store image for pc
        $pcimage = $this->candidateService->getImage($request->pcimg); // decode base64_decode
        $pcimage = Image::make($pcimage);
        $pcimage = $this->candidateService->storeImage($pcimage, 'pc'); // store image and return filename, directory
        $candidate->pcimage = $pcimage['filename'];

        //store  image for phone
        $phoneimage = $this->candidateService->getImage($request->phoneimg);
        $phoneimage = Image::make($phoneimage);
        $phoneimage = $this->candidateService->storeImage($phoneimage, 'phone'); // store image and return filename, directory
        $candidate->phoneimage = $phoneimage['filename'];

        $candidate->party_id = $request->party_id;
        $candidate->save();

        return redirect()->back()->with('status', 'Kandidati u krijua me sukses');
    }

    public function filterUnwanted(array $fields, $variable)
    {
        $keys = $this->getKeys($fields, $variable);
        return $result = Arr::except($variable, $keys);
    }
    public function getKeys(array $fields, $array)
    {

        $result = array();
        foreach($fields as $field){
            if (in_array($field, $array)){
            $key = array_search( $field, $array );
            $result[] = $key;
            }
        }
        return $result;
    }

    public function declareIfIsset($field, $request, $variableName)
    {
        if (isset($request->$field)){
            $variableName->$field = $request->$field;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
         return redirect()->route('admin.candidate.index' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parties = Party::all();
        $municipalities = $this->candidateService->getmunicipalities();
        sort($municipalities);
        $candidate = Candidate::find($id);
        return view('admin.candidate.edit')->with(['candidate' => $candidate , 'municipalities' => $municipalities , 'parties' => $parties ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidate $candidate)
    {
        Validator::make($request->all(), [
            'slug' =>  [
                'required',
                 Rule::unique('candidates')->ignore($candidate->id),
            ],
        ]);
        $candidate->slug = Str::slug($request->full_name);
        $fillableFields = $candidate->getFillable();
        $filteredFields = $this->filterUnwanted(['pcimg', 'phoneimg', 'party_id'], $fillableFields);
        foreach($filteredFields as $field => $value){
            $this->declareIfIsset($value, $request, $candidate);
        }

        //delete and store new image if isset
        if (isset($request->pcimg)){
            if (file_exists(public_path().'/images/pc/'.$candidate->pcimage)) {
                unlink(public_path().'/images/pc/'.$candidate->pcimage);
           }
            //store image for pc
            $pcimage = $this->candidateService->getImage($request->pcimg); // decode base64_decode
            $pcimage = Image::make($pcimage);
            $pcimage = $this->candidateService->storeImage($pcimage, 'pc'); // store image and return filename, directory
            $candidate->pcimage = $pcimage['filename'];
        }
        //delete and store new image if isset
        if (isset($request->phoneimg)){
            if (file_exists(public_path().'/images/phone/'.$candidate->phoneimage)) {
                unlink(public_path().'/images/phone/'.$candidate->phoneimage);
            }
        //store  image for phone
        $phoneimage = $this->candidateService->getImage($request->phoneimg);
        $phoneimage = Image::make($phoneimage);
        $phoneimage = $this->candidateService->storeImage($phoneimage, 'phone'); // store image and return filename, directory
        $candidate->phoneimage = $phoneimage['filename'];
        }

        $candidate->party_id = $request->party_id;
        $candidate->save();

        return redirect()->back()->with('status', "Kandidati u ndryshua me sukses");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidate $candidate)
    {
            //delete candidate pc image
            if (file_exists(public_path().'/images/pc/'.$candidate->pcimage)) {
                unlink(public_path().'/images/pc/'.$candidate->pcimage);
           }
            //delete candidate phone image
            if (file_exists(public_path() . '/images/phone/' . $candidate->phoneimage)) {
                unlink(public_path() . '/images/phone/' . $candidate->phoneimage);
            }
            $candidate->delete();
           return redirect()->back()->with('status', "Kandidati u fshi me sukses");
    }

    public function check_slug(Request $request)
    {
        if ($request->full_name == ''){
                return response()->json(['slug' => $request->full_name]);
        }
        // New version: to generate unique slugs
        $slug = SlugService::createSlug(Candidate::class, 'slug', $request->full_name);

        return response()->json(['slug' => $slug]);
    }
}
