<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Party;
use App\Http\Requests\PartyUpdate;
use App\Services\CandidateService;
use App\Http\Controller\Auth\Admin;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PartyController extends Controller
{

    protected $candidateService;

    public function __construct( CandidateService $candidateService){
      	$this->candidateService = $candidateService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $parties = Party::all();
        return view('admin.party.index', compact('parties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.party.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $party = new Party;
        $party->name = $request->name;
        $party->moto = $request->moto;
        $party->number = $request->number;
        $party->acronym = $request->acronym;
        $party->theme = $request->theme;
        $img = Image::make($request->file('logo'));
        $img->fit(895, 552, function ($constraint) {
            $constraint->upsize();
        });
        $logo = $this->candidateService->storeImage($img, 'party');
        $party->logo = $logo['filename'];
        $party->save();

        return redirect()->back()->with('status', 'Partia u krijua me sukses');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Party  $party
     * @return \Illuminate\Http\Response
     */
    public function show(Party $party )
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Party  $party
     * @return \Illuminate\Http\Response
     */
    public function edit(Party $party)
    {

        return view('admin.party.edit')->with(['party'=> $party]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Party  $party
     * @return \Illuminate\Http\Response
     */
    public function update($id, PartyUpdate $request)
    {

        $party = Party::find($id);

        $party->name = $request->name;
        $party->moto = $request->moto;
        $party->number = $request->number;
        $party->acronym = $request->acronym;
        $party->theme = $request->theme;
        if(isset($request->logo)){
            if (file_exists(public_path().'/images/party/'.$party->logo)) {
            unlink(public_path().'/images/party/'.$party->logo);
             }
            $img = Image::make($request->file('logo'));
            $img->fit(895, 552, function ($constraint) {
            $constraint->upsize();
            });
        $logo = $this->candidateService->storeImage($img, 'party');
        $party->logo = $logo['filename'];
        }

        $party->save();




         return redirect()->route('admin.parties.index')->with('status', 'Partia u ndryshua me sukses');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Party  $party
     * @return \Illuminate\Http\Response
     */
    public function destroy(Party $party)
    {

        if (file_exists(public_path().'/images/party/'.$party->logo)) {
            unlink(public_path().'/images/party/'.$party->logo);
             }
        $party->delete();
        return redirect()->route('admin.parties.index')->with('status', 'Partia u fshi me sukses');
    }
}
