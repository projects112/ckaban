<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Party;
use App\Models\Candidate;

use Illuminate\Http\Request;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home', [
            'party_count'=> Party::count(),
            'candidate_count' => Candidate::count(),
            'female_candidate_count' => Candidate::where('gender' , 'female')->count(),
            'male_candidate_count' => Candidate::where('gender' , 'male')->count(),
            

        ]);
    }
}
