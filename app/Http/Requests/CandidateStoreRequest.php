<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd($this->user);
        return [
            'pcimg' => 'required',
            'phoneimg' => 'required',
            'full_name' => 'required',
            'slug' => 'required|unique:candidates,slug,'
        ];
    }
    public function messages(){
        return[
            'pcimg.required'=>'PC-Image is required.',
            'phoneimg.required'=>'Phone-Image is required.',
        ];
    }
}
