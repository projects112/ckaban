<?php


namespace App\Services;
use App\Models\Candidate;


class CandidateService
{

    public function findCandidateBySlug($slug)
    {
        $slug = strtolower($slug);
        $candidate = Candidate::where('slug', $slug)->firstOrFail();
        return $candidate;
    }

    public function getImage($image)
    {
        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        return $image;
    }

    public function storeImage($image, $directoryName)
    {

         if (!is_dir('images/'.$directoryName)) {
            mkdir('images/'.$directoryName, 0700, true);
         }
        $filename= uniqid().'-'.time().'.png';
        $image->save("images/$directoryName/$filename");

        return ['filename' => $filename, 'directory' => $directoryName];
    }

    public function getmunicipalities()
    {
        return ['deçan',
'dragash',
'ferizaj',
'gjakovë',
'gjilan',
'drenas',
'istog',
'kaçanik',
'kamenicë',
'novobërdë',
'klinë',
'junik',
'lipjan',
'malishevë',
'mitrovicë',
'obiliq',
'pejë',
'podujevë',
'prishtinë',
'rahovec',
'prizren',
'shtërpcë',
'skënderaj',
'partesh',
'ranillug',
'kllokot',
'graçanicë',
'mamushë',
'mitrovicë e Veriut',
'fushë Kosovë',
'hani Elezit',
'suharekë',
'vushtrri',
'viti',
'shtime',
'gllogoc',
'leposaviq',
'zubin Potok',
'zveçan'];
    }
}
