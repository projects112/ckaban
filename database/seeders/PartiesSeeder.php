<?php

use Illuminate\Database\Seeder;

class PartiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('parties')->insert([
            'name'=> 'Partia Demokratike e Kosoves',
            'acronym'=> 'PDK',
            'number'=> '1',
            'moto'=> 'Moto 1',
            'logo'=> '60c1ca366f1fe-1623312950.png'
        ]);
        DB::table('parties')->insert([
            'name'=> 'Vetevendosje',
            'acronym'=> 'VV',
            'number'=> '2',
            'moto'=> 'Moto 2',
            'logo'=> '60c1ca18a3bef-1623312920.png'
        ]);
    }
}
