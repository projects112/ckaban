 function filterData(term) {
        const elements = document.querySelectorAll('h3 a');
        Array.from(elements)
            .filter((element) => !element.innerHTML.toLowerCase().includes(term))
            .forEach((element) => {
            parent = element.parentElement.parentElement.parentElement.parentElement.parentElement;
            parent.classList.add('hidden');
        });

        Array.from(elements)
            .filter((element) => element.innerHTML.toLowerCase().includes(term))
            .forEach((element) => {
            parent = element.parentElement.parentElement.parentElement.parentElement.parentElement;
            parent.classList.remove('hidden');
        });

    }
    const search = document.querySelector('.submit');
    search.addEventListener('keyup', (e) => {
       term = search.value.trim().toLowerCase();
       filterData(term);
    });
