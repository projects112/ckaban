 function revert(box, element) {
            element.style.backgroundColor = '#f2f2f2';
            box.style.color = '#666';
        }
        const cities = document.querySelectorAll("path");
        cities.forEach(city => {

            const text = city.parentElement.querySelector('text');
            city.addEventListener('mouseenter', e => {
                box = document.getElementById(text.innerHTML.trim().toLowerCase());
                element = box.parentElement.parentElement.parentElement;
                element.style.backgroundColor = '#72B626';
                element.style.transition = '.5s';
                box.style.color = '#ffffff';
                city.style.fill = '#32590e';
                text.style.color = 'black';
            });
            city.addEventListener('mouseleave', e => {
                box = document.getElementById(text.innerHTML.trim().toLowerCase());
                element = box.parentElement.parentElement.parentElement;
                revert(box, element);
                city.style.fill = '#72B626';
            });
            city.addEventListener('click', e => {
                box = document.getElementById(text.innerHTML.trim().toLowerCase());
                element = box.parentElement.parentElement.parentElement;
                revert(box, element);
                city.style.fill = '#72B626';
                window.location.href = '/komuna/' + text.innerHTML.trim().toLowerCase();
            });
            city.addEventListener('touchstart', e => {
                city.style.fill = '#32590e';
            });
            city.addEventListener('touchend', e => {
                city.style.fill = '#72B626';
            });

        })
