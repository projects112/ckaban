<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*
 *
 */
//  Route::group(['middleware' => 'forceSSL'], function() {

/* Auth start */

//Route::get('/password', function (){
//    return bcrypt('PLDnt%XCs@!*');
//});
Auth::routes(['register' => false]);
Route::get('/logout', 'Auth\LoginController@logout');
/* Auth end */

/* User start */
Route::get('/', 'HomeController@index')->name('home');

/* Admin panel start */
Route::group(['prefix' => 'admin/', 'namespace' => 'Admin', 'middleware' => 'auth', 'as' => 'admin.'], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('/parties', 'PartyController');
    Route::resource('/candidates', 'CandidateController');
    Route::get('/check_slug', 'CandidateController@check_slug')->name('check_slug');
});
/* Admin panel end */

Route::view('/wp-admin', 'wp-admin')->name('wp-admin');
Route::view('/cookiesprivacy', 'cookiesprivacy')->name('cookiesprivacy');
Route::view('/privacypolicy', 'privacypolicy')->name('privacypolicy');
Route::get('/test', 'HomeController@test');
Route::get('/contact-us', 'ContactController@index')->name('contact');
Route::post('/contact', 'ContactController@sendContact')->name('contact_us');
Route::get('/kandidatet', 'CandidateController@index')->name('kandidatet');
Route::get('/candidate/vote/add/{slug}', 'CandidateController@addVote')->name('candidate.addVote');


Route::get('/kandidatet/{query}', 'CandidateController@getAllAutocomplete')->name('getAllAutocomplete');
Route::get('/komunat/{municipality}', 'MunipicalityController@show')->name('munipicality.show');
Route::get('/komuna/{komuna}', 'MunipicalityController@showMunicipalityParties');
Route::get('/partite/', 'MunipicalityController@showAllParties')->name('parties.show');
Route::get('/partite/{id}', 'CandidateController@showPartyCandidates');
Route::get('/search/{party}/{query}', 'MunipicalityController@searchPartyCandidates');
Route::get('/search/{komuna}/{party}/{query}', 'MunipicalityController@searchMunicipalityCandidates');

//show party, candidate city
Route::get('/{komuna}/{party}', 'MunipicalityController@showMunicipalityCandidates');
Route::get('/{slug}', 'CandidateController@show')->name('candidate.show');
//  });



//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
