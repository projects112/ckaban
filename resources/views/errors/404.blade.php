@extends('layouts.app')
@section('pageTitle', 'Nuk u gjet')
@section('theme', 'green')
@section('page_theme', 'portfolio light')
@section('content')
<!-- Header Ends -->
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>error <span>404</span></h1>
    <span class="title-bg">Cka bon</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content text-center revealator-slideup revealator-once revealator-delay1">
    <h1>Faqja që ju kërkoni nuk është gjetur.</h1>

</section>
<!-- Main Content Ends -->
@endsection


