<header class="header" id="navbar-collapse-toggle">
    <!-- Fixed Navigation Starts -->
    <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
        <li class="icon-box {{ Request::is('/') ? 'active' : '' }}">
            <i class="fa fa-home"></i>
            <a href="{{route('home')}}">
                <h2>Ballina</h2>
            </a>
        </li>

        <li class="icon-box {{\Route::current()->getName() == 'kandidatet' ? 'active' : '' }}">
            <i class="fa fa-user"></i>
            <a href="{{route('kandidatet')}}">
                <h2>Kandidatët</h2>
            </a>
        </li>
        <li class="icon-box {{ \Route::current()->getName() == 'parties.show' ? 'active' : '' }}">
            <i class="fa fa fa-map-marker"></i>
            <a href="{{url('/partite')}}">
                <h2>Subjektet Politike</h2>
            </a>
        </li>
        <li class="icon-box {{ \Route::current()->getName() == 'contact' ? 'active' : '' }}">
            <i class="fa fa-envelope-open"></i>
            <a href="{{route('contact')}}">
                <h2>Kontakti</h2>
            </a>
        </li>
{{--        <li class="icon-box">--}}
{{--            <i class="fa fa-comments"></i>--}}
{{--            <a href="blog.html">--}}
{{--                <h2>Blog</h2>--}}
{{--            </a>--}}
{{--        </li>--}}
    </ul>
    <!-- Fixed Navigation Ends -->
    <!-- Mobile Menu Starts -->
    <nav role="navigation" class="d-block d-lg-none">
        <div id="menuToggle">
            <input type="checkbox" />
            <span></span>
            <span></span>
            <span></span>
            <ul class="list-unstyled" id="menu">
                <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{route('home')}}"><i class="fa fa-home"></i><span>Ballina</span></a></li>
                <li class="{{\Route::current()->getName() == 'kandidatet' ? 'active' : '' }}"><a href="{{route('kandidatet')}}"><i class="fa fa-user"></i><span>Kandidatët</span></a></li>
                <li class="{{ \Route::current()->getName() == 'parties.show' ? 'active' : '' }}"><a href="{{url('/partite')}}"><i class="fa fa fa-map-marker"></i><span>Subjektet Politike</span></a></li>
                <li class="{{ \Route::current()->getName() == 'contact' ? 'active' : '' }}"><a href="{{route('contact')}}"><i class="fa fa-folder-open"></i><span>Kontakti</span></a></li>
{{--                <li><a href="contact.html"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>--}}
{{--                <li><a href="blog.html"><i class="fa fa-comments"></i><span>Blog</span></a></li>--}}
            </ul>
        </div>
    </nav>
    <!-- Mobile Menu Ends -->
</header>
