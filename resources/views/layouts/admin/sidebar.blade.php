<div class="be-left-sidebar">
  <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle" href="/">Paneli Kryesor</a>
    <div class="left-sidebar-spacer">
      <div class="left-sidebar-scroll">
        <div class="left-sidebar-content">
          <ul class="sidebar-elements">
            <li class="divider">Menu</li>
            <li ><a href="/"><i class="icon mdi mdi-view-dashboard"></i><span>Faqja Kryesore</span></a></li>
            <li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="/admin"><i class="icon mdi mdi-home"></i><span>Paneli Kryesor</span></a></li>
            <li class="parent {{ Request::is('admin/parties*') ? 'open active' : '' }}">
              <a href="#"><i class="icon mdi mdi-book"></i><span>Partitë</span></a>
              <ul class="sub-menu">
                <li class="{{ Request::is('admin/parties') ? 'active' : '' }}"><a href="{{route('admin.parties.index')}}">Partitë</a></li>
                <li class="{{ Request::is('admin/parties/create') ? 'active' : '' }}"><a href="{{route('admin.parties.create')}}">Shto</a></li>
              </ul>
            </li>
            <li class="parent {{ Request::is('admin/candidates*') ? 'open' : '' }}"><a href="{{ url('admin.candidates.index') }}"><i class="icon mdi mdi-accounts"></i><span>Kandidatët</span></a>
              <ul class="sub-menu">
                 <li class="{{ Request::is('admin/candidates') ? 'active' : '' }}"><a href="{{route('admin.candidates.index')}}">Kandidatët</a></li>
                <li class="{{ Request::is('admin/candidates/create') ? 'active' : '' }}"><a href="{{route('admin.candidates.create')}}">Shto</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

