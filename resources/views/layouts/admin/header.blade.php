<nav class="navbar navbar-expand fixed-top be-top-header">
  <div class="container-fluid">
    <div class="be-navbar-header">
      <a href="/admin"> <img src="{{asset('img\tagDigitals.png')}}" style="width: 70%"> </a>
    </div>
    <div class="be-right-navbar">
      <ul class="nav navbar-nav float-right be-user-nav">
        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><img src="/img/avatar.png" alt="Avatar"><span class="user-name">{{Auth::user()->name}}</span></a>
          <div class="dropdown-menu" role="menu">
            <div class="user-info">
              <div class="user-name">{{Auth::user()->name}} </div>
            </div>
              <a class="dropdown-item" href="{{url('/user/edit')}}"><span class="icon mdi mdi-face"></span>Profili</a>

            <a class="dropdown-item" href="{{ url('/logout') }}"><span class="icon mdi mdi-power"></span>Logout</a>
          </div>
        </li>
      </ul>
         <!-- Fixed Navigation Ends -->
    <!-- Mobile Menu Starts -->
    <nav role="navigation" class="d-block d-lg-none">
        <div id="menuToggle">
            <input type="checkbox" />
            <span></span>
            <span></span>
            <span></span>
            <ul class="list-unstyled" id="menu">
                <li class="active"><a href="/light/index-2.html"><i class="fa fa-home"></i><span>Home</span></a></li>
                <li><a href="/light/about.html"><i class="fa fa-user"></i><span>About</span></a></li>
                <li><a href="/light/portfolio.html"><i class="fa fa-folder-open"></i><span>Portfolio</span></a></li>
                <li><a href="/light/contact.html"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>
                <li><a href="/light/blog.html"><i class="fa fa-comments"></i><span>Blog</span></a></li>
            </ul>
        </div>
    </nav>
    <!-- Mobile Menu Ends -->
    </div>
  </div>
</nav>
