<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="ckabon"
    content="Këtu mund ti gjeni të gjitha informatat e nevojshme për kandidatët politik dhe planet për zgjedhjet 2021">
    <meta name="kandidatet"
    content="Shiko të gjith kandidatët e përzgjedhur nga gjdo parti politike në Republikën e Kosovës, ose kandidatët e pavarur">
    <meta name="partitë"
    content="Shiko të gjitha partitë dhe kandidatët e tyre për zgjedhjet 2021">
    <meta name="Qytetet"
    content="Shiko të gjith kandidatët për komunën tënde vetëm me dy klikime">
    <title>@yield('pageTitle')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('light\img\ckabonLogo.png')}}" sizes="52x52">

    <!-- Seo optimize -->
    <meta property="og:site_name" content="Ckabon.net">
    <meta name="keywords" content="ckabon, cka bon, Cka bon, CkaBon, Ckabon, cabon, ckaban, qkaban, qkabon, partite, parti, kosova, kosovo, subject, pdk, ldk, vv, aak, akr">
    <meta name="description" content="Platformë digjitale.">

    <!-- Template Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,600i,700" rel="stylesheet">

    <!-- Template CSS Files -->
    <link href="/light/css/bootstrap.min.css" rel="stylesheet">
    <link href="/light/css/preloader.min.css" rel="stylesheet">
    <link href="/light/css/circle.css" rel="stylesheet">
    <link href="/light/css/font-awesome.min.css" rel="stylesheet">
    <link href="/light/css/fm.revealator.jquery.min.css" rel="stylesheet">
    <link href="/light/css/style.css" rel="stylesheet">

    <!-- CSS Skin File -->
    <link href="/light/css/skins/@yield('theme').css" rel="stylesheet">

    <!-- Live Style Switcher - demo only -->
    <link rel="alternate stylesheet" type="text/css" title="blue" href="/light/css/skins/blue.css" />
    <link rel="alternate stylesheet" type="text/css" title="green" href="/light/css/skins/green.css" />
    <link rel="alternate stylesheet" type="text/css" title="yellow" href="/light/css/skins/yellow.css" />
    <link rel="alternate stylesheet" type="text/css" title="blueviolet" href="/light/css/skins/blueviolet.css" />
    <link rel="alternate stylesheet" type="text/css" title="goldenrod" href="/light/css/skins/goldenrod.css" />
    <link rel="alternate stylesheet" type="text/css" title="magenta" href="/light/css/skins/magenta.css" />
    <link rel="alternate stylesheet" type="text/css" title="orange" href="/light/css/skins/orange.css" />
    <link rel="alternate stylesheet" type="text/css" title="purple" href="/light/css/skins/purple.css" />
    <link rel="alternate stylesheet" type="text/css" title="red" href="/light/css/skins/red.css" />
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="/light/css/skins/yellowgreen.css" />
    <link rel="alternate stylesheet" type="text/css" title="aak" href="/light/css/skins/aak.css" />
    <link rel="alternate stylesheet" type="text/css" title="pdk" href="/light/css/skins/pdk.css" />
    <link rel="alternate stylesheet" type="text/css" title="ldk" href="/light/css/skins/ldk.css" />
    <link rel="stylesheet" type="text/css" href="/light/css/styleswitcher.css" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,600i,700" rel="stylesheet">

    {{-- Google analytics link --}}
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-2FX7WKRZWH"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-2FX7WKRZWH');
        </script>
    <!-- Modernizr JS File -->
    <script src="/light/js/modernizr.custom.js"></script>
    <style>
        .cookies{
            z-index: 16;
            width: 100%;
        }
        body{
            z-index: 2;
            font-family: 'Poppins', sans-serif;
            font-size: 15px;
            font-weight: 500;
        }
        /*section .title-section{*/
        /*    z-index: 2;*/
        /*}*/
    </style>
    @yield('custom_header')
    @yield('custom_style')

</head>

<body class="@yield('page_theme')">
     <div class="cookies" id="hidden" style="display: none; position: fixed;@yield('cookiesStyle')">
    <!-- START Bootstrap-Cookie-Alert -->
    <div class="btn alert text-center cookiealert" style="background-color: #eff0f0; width: 100%; " role="alert">
      <!--  &#x1F36A; -->
      Duke përdorur shërbimet e Cka-Bon ju pajtoheni me përdorimin tonë të Cookies. Ne operojmë në të gjithë botën dhe përdorim cookies për statistika, personalizime dhe reklama.
      <a href="{{url('/cookiesprivacy')}}" target="_blank"><span>Lexo më shumë...</span></a>

      <button type="button" class="btn btn-success btn-sm" onclick="clicked()">
        Pranoj
      </button>
    </div>
    <!-- END Bootstrap-Cookie-Alert -->
    <script src="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.js"></script>
    <script type="text/javascript">
      let accepted = localStorage.getItem("pranoi");
      console;
      if (!accepted) {
        var x = document.getElementById("hidden");
        x.style.display = "block";
      }
      function clicked() {
        localStorage.setItem("pranoi", true);
        x.style.display = "none";
      }

    </script>
    </div>
<!-- Header Starts -->
@include('layouts.header')
<!-- Header Ends -->
<!-- Main Content Starts -->
@yield('content')
<!-- Main Content Ends -->

<!-- Template JS Files -->
<script src="/light/js/jquery-3.5.0.min.js"></script>
<script src="/light/js/styleswitcher.js"></script>
{{--<script src="/light/js/preloader.min.js"></script>--}}
<script src="/light/js/fm.revealator.jquery.min.js"></script>
<script src="/light/js/imagesloaded.pkgd.min.js"></script>
<script src="/light/js/masonry.pkgd.min.js"></script>
<script src="/light/js/classie.js"></script>
<script src="/light/js/cbpGridGallery.js"></script>
<script src="/light/js/jquery.hoverdir.js"></script>
<script src="/light/js/popper.min.js"></script>
<script src="/light/js/bootstrap.js"></script>
<script src="/light/js/custom.js"></script>
@yield('custom_footer')
</body>


</html>
