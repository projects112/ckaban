@extends('layouts.app')
@section('pageTitle', 'Kandidatet')
@section('page_theme', 'blog light')

@section('theme', 'green')
@section('custom_header')
    <meta property="og:image" content="{{asset('light\img\og.jpg')}}"/>
    <style>
        .contactform input[type=text] {
            border: 1px solid #111;
            width: 100%;
            background: #252525;
            color: #fff;
            padding: 15px 26px;
            border-radius: 30px;
            outline: none !important;
            transition: .3s;
        }
        .dropdown-item {
            border-radius: 25px;
        }
    </style>
@endsection
@section('content')
    <!-- Page Title Starts -->
    <section
        class="title-section text-left text-sm-center revealator-slideup revealatocandidate->r-once revealator-delay1">
        <h1><span>kandidatet</span></h1>
        <span class="title-bg">Ckabon</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <div class="contactform" style="margin: 0 auto">
                <div class="row">
                    <div class="col-12 col-md-12 m-auto" style="text-align: center;">

                        <h6>Profilet e prezentuara në platformë janë fytyra publike-politike, mirpo disa prej tyre nuk janë domosdoshmërisht kandidatë për Zgjedhjet Lokale 2021. Emrat dhe informatat e tyre janë paraqitur në mënyrë të paautorizuar për të shërbyer si shembuj se si funksionon platforma CKA-BON.</h6>
                    </div>
                </div>
            </div>
            <div class="contactform" style="margin: 0 auto">
                <div class="row">
                    <div class="col-12 col-md-8 m-auto">
                        <input type="text" name="search" class="submit" autocomplete="off" placeholder="Search">

                        <div class="dropdown ">
                            <div class="dropdown-menu col-md-12" style="border-radius:25px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Articles Starts -->
            <div class="row" style="margin-top: 25px;">
                <!-- Article Starts -->
                <?php $detect = new Mobile_Detect();?>
                @foreach($candidates as $candidate)
                    @if(!$detect->isMobile())
                        <div class="col-12 col-md-3 col-lg-3 col-xl-3 mb-30">
                            <article class="post-container">
                                <div class="post-thumb">
                                    <a href="{{url("/$candidate->slug")}}"
                                       class="d-block position-relative overflow-hidden">
                                        <img src="{{asset('images/phone/'.$candidate->phoneimage)}}" width="255"
                                             height="230" class="img-fluid" alt="{{$candidate->full_name}}">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <div class="entry-header">
                                        <h3 style="text-transform: capitalize"><a
                                                href="{{url("/$candidate->slug")}}">{{$candidate->full_name}}<span
                                                    style="color: #72b626"> {{$candidate->number}}</span></a></h3>

                                        <h6 style="text-transform: uppercase;">
                                            {{ $candidate->party ? $candidate->party->acronym. ' -' : ''}} <span
                                                style="text-transform: capitalize">{{$candidate->municipality}}</span>
                                        </h6>
                                    </div>
                                </div>
                            </article>
                            @else
                                <div style="margin: 0 auto; padding-top: 15px;">
                                    <article class="post-container" style="width: 250px;">
                                        <div class="post-thumb">
                                            <a href="{{url("/$candidate->slug")}}"
                                               class="d-block position-relative overflow-hidden">
                                                <img src="{{asset('images/phone/'.$candidate->phoneimage)}}" width="255"
                                                     height="230" class="img-fluid" alt="{{$candidate->full_name}}">
                                            </a>
                                        </div>
                                        <div class="post-content">
                                            <div class="entry-header">
                                                <h3 style="text-transform: capitalize"><a
                                                        href="{{url("/$candidate->slug")}}">{{$candidate->full_name}}
                                                        <span style="color: #72b626"> {{$candidate->number}}</span></a>
                                                </h3>

                                                <h6 style="text-transform: uppercase;">
                                                    {{ $candidate->party ? $candidate->party->acronym. ' -' : ''}} <span
                                                        style="text-transform: capitalize">{{$candidate->municipality}}</span>
                                                </h6>
                                            </div>
                                        </div>
                                    </article>
                                    @endif
                                </div>
                            @endforeach
                            <!-- Article Ends -->
                                <!-- Pagination Starts -->
                                <div class="col-12 mt-4">

                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination justify-content-center mb-0">
                                        {{$candidates->links()}}
{{--                                            <li class="page-item"><a class="page-link" href="#">1</a></li>--}}
{{--                                            <li class="page-item active"><a class="page-link" href="#">2</a></li>--}}
{{--                                            <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
{{--                                            <li class="page-item"><a class="page-link" href="#">4</a></li>--}}
                                        </ul>
                                    </nav>
                                </div>
                                <!-- Pagination Ends -->
                        </div>
                        <!-- Articles Ends -->
            </div>
        </div>
    </section>


    {{-- @if($detect->isMobile())--}}
    <footer class="footer " style="
  pflex-grow: 0;
  flex-shrink: 0;
  flex-basis: auto;
  padding: 5px;
  text-align: center;">Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/en">TAG
            Digitals</a>' ©
        <script>document.write(new Date().getFullYear())</script>
    </footer>
    {{-- @else--}}
    {{--<footer class="footer mt-auto py-3" style=" background: transparent;--}}
    {{--                                            hight: 60px;--}}
    {{--                                            width: 100%;--}}
    {{--                                            margin: 0 auto;--}}
    {{--                                            position: absolute;--}}
    {{--                                            bottom: 0;--}}
    {{--                                            clear: both;">--}}
    {{--  <div class="container text-center">--}}
    {{--    <span >--}}
    {{--    Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/">TAG Digitals</a>' © <script>document.write(new Date().getFullYear())</script></span>--}}
    {{--  </div>--}}
    {{--</footer>--}}
    {{--@endif--}}

@endsection

@section('custom_footer')
    <script>
        const dropdown = document.querySelector('.dropdown-menu');


        const search = document.querySelector('.submit');
        search.addEventListener('keyup', e => {
            const query = search.value.trim()

            $.get(`/kandidatet/${query}`,
                function (data) {
                    if (query != '') {
                        if (data['count'] !== 0) {
                            dropdown.style.display = 'block';
                        } else {
                            dropdown.style.display = 'none';
                        }
                        dropdown.innerHTML = data[0];
                    }else {
                        dropdown.style.display = 'none';
                    }
                }
            ).then(e => {
                const dropdownItems = dropdown.querySelectorAll('a');
                dropdownItems.forEach(item => {
                    item.addEventListener('mousedown', e => {
                        item.style.backgroundColor = '#72b626';
                    });
                    item.addEventListener('mouseup', e => {
                        console.log('ss');
                        item.style.backgroundColor = '#ffffff';
                    });
                    item.addEventListener('mouseleave', e => {
                        console.log('ss');
                        item.style.backgroundColor = '#ffffff';
                    });
                });
            });

        });
    </script>
@endsection

