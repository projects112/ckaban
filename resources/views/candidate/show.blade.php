@extends('layouts.app')
@section('pageTitle', $candidate->full_name)

{{--set page theme color if isset else set to green--}}
@section('theme', $candidate->party ? $candidate->party->theme : 'green' )
{{--@section('theme', 'green' )--}}
@section('page_theme', 'home light')
@section('custom_style')
    <style>


    </style>
@section('cookiesStyle', 'position: absolute; top: 0;left: 0;')
@endsection
@section('custom_header')
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500&display=swap" rel="stylesheet">
    <script type="text/javascript" src="{{asset('/qrcode/qrcode.js')}}"></script>
    <script type="text/javascript" src="/qrcode/jquery.min.js"></script>
    <script src="/jsqrcode/easy.qrcode.min.js" type="text/javascript" charset="utf-8"></script>
@endsection
@section('content')

    <section
        class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
        <div class="color-block d-none d-lg-block"></div>
        <div class="row home-details-container align-items-center">

            <div class="col-lg-4 bg position-fixed d-none d-lg-block"
                 style="background-image: url('images/pc/{{$candidate->pcimage}}')"></div>
            <img src="{{asset('images/phone/'.$candidate->phoneimage)}}"
                 alt="{{$candidate->full_name}}" class="img-fluid main-img-mobile d-sm-block d-lg-none"
                 style="margin-bottom: -25px;" alt="my picture"/>
            <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left mt-5 d-md-block">
                <div style="margin-bottom: 15px;">

                    <h1 class="text-uppercase poppins-font""><span
                            style="color: #72B626"> </span>
                        <span>{{$candidate->full_name}}</span></h1>
                <?php $detect = new Mobile_Detect();?>

                @if(!$detect->isMobile())
                    @if(strlen($candidate->short_bio) > 570 )
                          <div id="bioPC" style="overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 5;-webkit-box-orient: vertical;">
                              <p id="shortBioPC" class="open-sans-font" style="text-align:justify">   {!!  $candidate->short_bio !!} </p></div><a id="seeMoreBioPC" href="#" onclick="seeMoreBioPC()">  Shiko më shumë</a>
                    @else
                        <div style="overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-box-orient: vertical;">
                            <p id="shortBioPC" class="open-sans-font" style="text-align:justify"> {!! $candidate->short_bio!!} </p>
                        </div>
                    @endif
                @else
                    @if(strlen($candidate->short_bio) > 160 )
                        <div id="bio" style="  overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 3; /* number of lines to show */-webkit-box-orient: vertical;">
                            <p id="shortBio" class="open-sans-font" style="text-align:left;"> {!!$candidate->short_bio !!} </div> <a href="#" id="seeMoreBio" onclick="seeMoreBio()">  Shiko më shumë</a> </p>
                    @else
                            <p id="shortBio" class="open-sans-font" style="text-align:left;"> {!! $candidate->short_bio!!} </p>
                    @endif
                @endif

{{--                    @if(!$detect->isMobile())--}}
{{--                        @if(isset($candidate->facebook) or isset($candidate->instagram) or isset($candidate->twitter) or isset($candidate->email))--}}
{{--                            <ul class="social list-unstyled">--}}
{{--                                <li class="facebook {{ isset($candidate->facebook)? '' : 'hidden' }}"><a target="_blank" title="Facebook" href="{{ isset($candidate->facebook)? $candidate->facebook : '#' }}"><i class="fa fa-facebook"></i></a>--}}
{{--                                </li>--}}
{{--                                <li class="instagram {{ isset($candidate->instagram)? '' : 'hidden' }} "><a target="_blank" title="instagram" href="{{ isset($candidate->instagram)? $candidate->instagram : '#' }}"><i class="fa fa-instagram"></i></a>--}}
{{--                                </li>--}}
{{--                                <li class="instagram {{ isset($candidate->twitter)? '' : 'hidden' }} "><a target="_blank" title="twitter" href="{{ isset($candidate->twitter)? $candidate->twitter : '#' }}"><i class="fa fa-twitter"></i></a>--}}
{{--                                </li>--}}
{{--                                <li class="dribbble {{ isset($candidate->email)? '' : 'hidden' }}"><a target="_blank" title="Mail" href="{{ isset($candidate->email)? 'mailto:'.$candidate->email : '#' }}"><i class="fa fa-envelope"></i></a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        @endif--}}
{{--                    @endif--}}
                    <div class="ckaban">
                        <h1 class="text-uppercase poppins-font">
                            Cka-bon
                            <span style="font-size: 30px; text-transform: none; font-family: 'Caveat', cursive;">kur t'fiton?</span>
                        </h1>
                    </div>

                     @if(!$detect->isMobile())
                         @if(strlen($candidate->ckaban) > 570 )
                            <div id="bioCkaPc" style="overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
                            <p id="ckabanPC" class="open-sans-font" style="text-align:justify">{!! $candidate->ckaban !!}</p></div><a href="#" id="seeMoreCkaPC" onclick="seeMoreCkaPC()"> Shiko më shumë</a>
                        @else
                            <p id="ckabanPC" class="open-sans-font" style="text-align:justify">{!! $candidate->ckaban !!}</p>
                        @endif
                   @else

                        @if(strlen($candidate->ckaban) > 160 )
                            <div id="bioCka" style="overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
                                <p id="ckaban" class="open-sans-font" style="text-align:left;">{!! $candidate->ckaban !!}</p></div><a href="#" id="seeMoreCka" onclick="seeMoreCka()"> Shiko më shumë</a>
                         @else
                            <p id="ckabanPC" class="open-sans-font" style="text-align:justify">{{ $candidate->ckaban }}</p>
                        @endif
                    @endif

                </div>
                @if(!$detect->isMobile())
                    <div class="col-12 col-lg-12 col-xl-12 mt-lg-0" style="text-align: center; ">
                        <div class="row">
                            {{--                    <div class="col-3"></div>--}}
                             @if($candidate->party)
                                <div class="col-4">
                                    <div class="box-stats">
                                        <p class="open-sans-font m-0 position-relative text-uppercase">Voto <strong> {{$candidate->party->acronym}}</strong>
                                        </p>
                                        <p class="open-sans-font m-0 position-relative text-uppercase"></p>
                                        <h3 class="poppins-font position-relative">{{$candidate->party->number}}</h3>
                                    </div>
                                </div>
                            @endif


                           <div class="d-block d-lg-block m-auto " id="qrcode" ></div>
                                <div class="col-4">
                                    <div class="box-stats">
                                        <p class="open-sans-font m-0 position-relative text-uppercase"><a class="btn text-white disabled" id="voteBtn"  href="{{route('candidate.addVote', "$candidate->slug")}}"> Voto: </a></p>
                                        <h3 class="poppins-font position-relative">{{$candidate->number}}</h3>
                                    </div>
                                      <span>Votat: <span id="voteTxt">{{$candidate->votes ?? ''}}</span></span>
                                </div>
                        </div>
                    </div>
                @else

                    <div class="col-12 col-lg-12 col-xl-12 mt-lg-0" style="text-align: center">
                        <div class="row">
                            @if($candidate->party)
                                <div class="col-6" style="padding-left: 0px; padding-right: 10px">
                                    <div class="box-stats">
                                        <p class="open-sans-font m-0 position-relative text-uppercase">Voto <strong> {{$candidate->party->acronym}}</strong>
                                        </p>
                                        <p class="open-sans-font m-0 position-relative text-uppercase"></p>
                                        <h3 class="poppins-font position-relative">{{$candidate->party->number}}</h3>
                                    </div>
                                </div>
                            @endif

                            <div class="col-6" style="padding-left: 10px; padding-right: 0px">
                                <div class="box-stats">
                                 <p class="open-sans-font m-0 position-relative text-uppercase"><a class="btn btn-sm text-white disabled" id="voteBtn" href="{{route('candidate.addVote', "$candidate->slug")}}"> Voto: </a></p>
                                    <h3 class="poppins-font position-relative">{{$candidate->number}}</h3>
                                </div>
                            <span id="voteTxt">Votat: <span id="voteTxt">{{$candidate->votes ?? ''}}</span></span>
                            </div>

                        </div>
                    </div>
                @endif
                     <br>
                        @if(isset($candidate->facebook) or isset($candidate->instagram) or isset($candidate->twitter) or isset($candidate->email))
                            <ul style="text-align: center" class="social list-unstyled">
                                <li class="facebook {{ isset($candidate->facebook)? '' : 'hidden' }}"><a target="_blank" title="Facebook" href="{{ isset($candidate->facebook)? $candidate->facebook : '#' }}"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="instagram {{ isset($candidate->instagram)? '' : 'hidden' }} "><a target="_blank" title="instagram" href="{{ isset($candidate->instagram)? $candidate->instagram : '#' }}"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li class="instagram {{ isset($candidate->twitter)? '' : 'hidden' }} "><a target="_blank" title="twitter" href="{{ isset($candidate->twitter)? $candidate->twitter : '#' }}"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="dribbble {{ isset($candidate->email)? '' : 'hidden' }}"><a target="_blank" title="Mail" href="{{ isset($candidate->email)? 'mailto:'.$candidate->email : '#' }}"><i class="fa fa-envelope"></i></a>
                                </li>
                            </ul>

                        @endif
                            	{{-- dont show footer for mobile --}}
	                        	@if(!$detect->isMobile())
		                            <ul style="text-align: center">
		                                <li style="list-style-type: none;">Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/">TAG Digitals</a>' © <script>document.write(new Date().getFullYear())</script>
		                                </li>
		                            </ul>
	                            @endif


            </div>

        </div>
    </section>

    {{-- show footer if is on mobile --}}
     @if($detect->isMobile())
		   <footer class="footer " style="
		  pflex-grow: 0;
		  flex-shrink: 0;
		  flex-basis: auto;
		  padding: 5px;
		  text-align: center;">Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/en">TAG Digitals</a>' © <script>document.write(new Date().getFullYear())</script>
		</footer>
	@endif
@endsection

@section('custom_footer')
    <script type="text/javascript">
        /* ----------------------------------------------------------- */
		/*  Vote
        /* ----------------------------------------------------------- */
        var voteBtn = document.getElementById('voteBtn');
        var voted = localStorage.getItem('voted') ? localStorage.getItem('voted') : false;

        if (!voted){
            voteBtn.classList.remove('disabled');
        }
        voteBtn.addEventListener('click', () => {
            localStorage.setItem('voted', 'true');
            voteBtn.classList.add('disabled');
        })

        /* ----------------------------------------------------------- */
		/*  Vote end
        /* ----------------------------------------------------------- */


        /* ----------------------------------------------------------- */
		/*  Phone See more,less bio and ckaban start
        /* ----------------------------------------------------------- */
        var myDivP = $('#bio');
        var styleP = `overflow: hidden;
                   text-overflow: ellipsis;
                   display: -webkit-box;
                   -webkit-line-clamp: 3;
                   -webkit-box-orient: vertical;`;
        function seeMoreBio() {
             myDivP.removeAttr('style');
             $("#seeMoreBio").attr('onclick', 'seeLessBio()');
             $("#seeMoreBio").html('Shiko më pak');
        }
        //see less
        function seeLessBio(){
             myDivP.attr('style', stylePC);
             $("#seeMoreBio").attr('onclick', 'seeMoreBio()');
             $("#seeMoreBio").html('Shiko më shumë');
        }



        var myDivCka = $('#bioCka');
        var styleCka = `overflow: hidden;
                   text-overflow: ellipsis;
                   display: -webkit-box;
                   -webkit-line-clamp: 3;
                   -webkit-box-orient: vertical;`;
        function seeMoreCka() {
             myDivCka.removeAttr('style');
             $("#seeMoreCka").attr('onclick', 'seeLessCka()');
             $("#seeMoreCka").html('Shiko më pak');
        }
        function seeLessCka(){
             myDivCka.attr('style', styleCka);
             $("#seeMoreCka").attr('onclick', 'seeMoreCka()');
             $("#seeMoreCka").html('Shiko më shumë');
        }

        /* ----------------------------------------------------------- */
		/*  Phone See more,less bio and ckaban end
        /* ----------------------------------------------------------- */




        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //pc
        var myDivPC = $('#bioPC');
        var stylePC = `overflow: hidden;
                   text-overflow: ellipsis;
                   display: -webkit-box;
                   -webkit-line-clamp: 6;
                   -webkit-box-orient: vertical;`;
        function seeMoreBioPC() {
            console.log(myDivPC.html())
             myDivPC.removeAttr('style');
             $("#seeMoreBioPC").attr('onclick', 'seeLessBioPC()');
             $("#seeMoreBioPC").html('Shiko më pak');
        }

        //see less
        function seeLessBioPC(){
             myDivPC.attr('style', stylePC);
             $("#seeMoreBioPC").attr('onclick', 'seeMoreBioPC()');
             $("#seeMoreBioPC").html('Shiko më shumë');
        }

        /////////////////////////////////////////////////////////////

        var myDivCkaPC = $('#bioCkaPc');
        var styleCkaPC = `overflow: hidden;
               text-overflow: ellipsis;
               display: -webkit-box;
               -webkit-line-clamp: 6;
               -webkit-box-orient: vertical;`;
        function seeMoreCkaPC() {
             myDivCkaPC.removeAttr('style');
             $("#seeMoreCkaPC").attr('onclick', 'seeLessCkaPC()');
             $("#seeMoreCkaPC").html('Shiko më pak');
        }
        function seeLessCkaPC(){
             myDivCkaPC.attr('style', styleCkaPC);
             $("#seeMoreCkaPC").attr('onclick', 'seeMoreCkaPC()');
             $("#seeMoreCkaPC").html('Shiko më shumë');
        }


        let color = $('.home h1').css(['color']).color;
        var options = {
            text: "{{\Request::url()}}", // Content

            width: 120, // Widht
            height: 120, // Height
            colorDark: color, // Dark color
            colorLight: "#ffffff", // Light color

            // quietZone: 5,
            // quietZoneColor: '#69973f',

            // === Logo
            logo: "/light/img/k.png", // LOGO
            logoBackgroundColor: color, // Logo backgroud color, Invalid when `logBgTransparent` is true; default is '#ffffff'
            logoBackgroundTransparent: false, // Whether use transparent image, default is false
            logoWidth: 50, // fixed logo width. default is `width/3.5`
            logoHeight: 50,

            correctLevel: QRCode.CorrectLevel.H // L, M, Q, H
        };


        // Create QRCode Object
        new QRCode(document.getElementById("qrcode"), options);
    </script>
@endsection

