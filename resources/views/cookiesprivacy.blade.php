@extends('layouts.app')
@section('pageTitle','CookiesPrivacy')
@section('page_theme', 'blog light')

@section('theme', 'green')
 <style type="text/css">
      body {
        background-color: #eff0f0;
      }

      h2,
      h3,
      h5 {
        text-align: center;
      }
      p {
        text-align: justify;
      }
    </style>

@section('content')

<div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Cookies</h2>
          <p>
            Cka Bon përdor teknologjinë përcjellëse (“cookies” – dosje të vogla
            tekstesh që ruhet në kompjuterin tuaj) që implementohet në sistemet
            e reklamimit dhe i llogarit vizitorët unik. Kjo na mundëson neve
            mbledhjen e statistikave rreth vizitorëve tanë për përdorim të
            brendshëm si dhe për t’u siguruar se ju nuk do të shihni të njejtat
            hapsira reklamuese gjatë gjithë kohës që ju jeni në faqen tonë të
            internetit. Teknologjia përcjellëse është anonime dhe nuk përmban të
            dhëna personale.
          </p>
          <h3>Të dhënat duke përdorur cookies</h3>
          <p>
            Cookie eshte nje fajll standard qe vendoset në kompjuterin apo
            pajisjen e juaj. Ne mund te përdorim cookies apo teknologjitë e
            ngjashme për mbledhjen e informatave në lidhje me faqet që ju
            shikoni, linqet që ju klikoni, dhe veprimet e tjera që ju ndërmerrni
            në shërbimet tona, brenda reklamave tona apo përmbajtjes së e-mailit
            ne mënyrë që të ofrojmë shërbime sa më relevante. Edhe pse shumica e
            shfletuesve të internetit i pranojnë automatikisht cookies, ato
            gjithashtu ofrojnë mundësi të pranimit, lajmërimit apo refuzimit të
            tyre në momentin që tentohet vendosja e tyre në kompjuter apo
            pajisje. Gjithsesi një pjese e shërbimeve tona mund të mos
            funksionoje si duhet nëse ju i refuzoni cookies. Kur shfletuesi i
            juaj i internetit e lejon vendosjen e cookies, ne i përdorim edhe
            cookies për mirëmbajtje të sesionit edhe cookies të vazhdueshme në
            mënyrë që të kuptojmë më mirë se si ndërveproni me shërbimet tona,
            si: ofrimin e sigurisë se llogarisë, personalizimin e përmbajtjes qe
            ju shfaqim përfshirë reklamat apo ruajtjen e preferencave te juaja.
          </p>

          <h3>Të dhënat automatike</h3>
          <h5>Informata që lidhen me ndërveprimin tuaj me shërbimet tona</h5>
          <p>
            Preferencat e juaja të marketingut, reklamave dhe komunikimit tuaj
            me ne. Këtë informacion ne e pranojmë nga pajisjet që ju i përdorni
            kur shfrytëzoni shërbimet tona, regjistroheni për një llogari te ne,
            përditësoni apo shtoni informata në llogarinë tuaj, participoni në
            diskutime apo chat-e, apo kur ju komunikoni me ne në lidhje me
            shërbimet tona. Ky informacion përfshin si vijon: ID-në e pajisjes
            tuaj apo numrin unik identifikues dhe llojin e pajisjes, sistemin
            operativ apo të ngjashme. <br />
            Informata mbi vendndodhjen gjeografike, nga IP adresa apo të
            shënimeve tjera të pajisjes. <br />
            Informatat e kompjuterit, siç mund të jenë statistikat e shikimit të
            faqeve nga ana e juaj, qarkullimi për apo nga sajtet e caktuara,
            linqeve, referimet URL, IP adresat tuaja, historinë tuaj të
            shfletimeve apo të dhënat tuaja të kyçjes në ueb faqet tona, apo
            partnereve tanë. <br />
            Termet e kërkimit apo filterët e përdorur.
          </p>
        </div>
        <div class="col-md-3"></div>
        <a
          type="button"
          class="btn btn-success col-md-6"
          style="margin-bottom: 20px"
          href="https://ckabon.net/">Vazhdo me faqen kryesore</a>

        <br />
      </div>
    </div>


@endsection

@section('custom_footer')

@endsection
