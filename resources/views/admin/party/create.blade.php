@extends('layouts.admin.admin')

@section('pageTitle', 'Shto Parti')

@section('content')
<div class="row justify-content-center">
  <div class="col-lg-6">
    <div class="card card-border-color card-border-color-primary">
      <div class="card-header card-header-divider">Shto Parti</div>
      <div class="card-body">
        <form action="{{route('admin.parties.store')}}" method="POST" data-parsley-validate="" novalidate="" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label>Emri i Partisë</label>
              <input type="text" name="name" parsley-trigger="change" required="" placeholder="Emri" autocomplete="off" class="form-control" value="{{old('name')}}">
            </div>
            <div class="form-group">
              <label>Shkurtesa e Partisë</label>
              <input type="text" name="acronym" parsley-trigger="change" required="" placeholder="VV" autocomplete="off" class="form-control" value="{{old('acronym')}}">
            </div>
            <div class="form-group">
              <label>Moto e Partisë</label>
                <input type="text" name="moto" parsley-trigger="change" required="" placeholder="Moto..." autocomplete="off" class="form-control" {{old('moto')}}>
            </div>
            <div class="form-group">
              <label>Numri i Partisë</label>
                <input type="number" name="number" min="0" parsley-trigger="change" required="" placeholder="1 2..." autocomplete="off" class="form-control" {{old('number')}}>
            </div>
            <div class="form-group">
              <label>Logo e Partisë</label>
              <input type="file" required="" name="logo" class="form-control">
            </div>
            <div class="form-group">
                <label>Ngjyra<span class="text-danger">*</span></label>
                <select class="form-control" name="theme">
                    <option selected disabled>Zgjidh ngjyren...</option>
                        <option value="red">red</option>
                        <option value="blue">blue</option>
                        <option value="blueviolet">blueviolet</option>
                        <option value="goldenrod">goldenrod</option>
                        <option value="green">green</option>
                        <option value="magenta">magenta</option>
                        <option value="purple">purple</option>
                        <option value="yellow">yellow</option>
                        <option value="yellowgreen">yellowgreen</option>
                        <option value="aak">aak - darkblue</option>
                        <option value="pdk">pdk - lightblue</option>
                </select>
                <img src="/light/img/styleswitcher/red.png" alt="red"/>
                <img src="/light/img/styleswitcher/blue.png" alt="blue"/>
                <img src="/light/img/styleswitcher/blueviolet.png" alt="blueviolet"/>
                <img src="/light/img/styleswitcher/goldenrod.png" alt="goldenrod"/>
                <img src="/light/img/styleswitcher/green.png" alt="green"/>
                <img src="/light/img/styleswitcher/magenta.png" alt="magenta"/>
                <img src="/light/img/styleswitcher/purple.png" alt="purple"/>
                <img src="/light/img/styleswitcher/yellow.png" alt="yellow"/>
                <img src="/light/img/styleswitcher/yellowgreen.png" alt="yellowgreen"/>
            </div>
            <p class="text-right">
              <button type="submit" class="btn btn-space btn-primary">Submit</button>
            </p>
      </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_footer')
    <script src="/js/app-form-elements.js" type="text/javascript"></script>
    <script src="/js/main.js" type="text/javascript"></script>
    <script src="/lib/parsley/parsley.min.js" type="text/javascript"></script>
@endsection

@section('custom_scripts')

@endsection
