@extends('layouts.admin.admin')

@section('pageTitle', 'Ndrysho Partinë')
@section('content')
<div class="row justify-content-center">
  <div class="col-lg-6">
    <div class="card card-border-color card-border-color-primary">
      <div class="card-header card-header-divider">Ndrysho Partinë</div>
      <div class="card-body">
        <form action="{{route('admin.parties.update', $party)}}" method="POST" data-parsley-validate="" novalidate="" enctype="multipart/form-data">
            @csrf
            @method('PUT')
             <div class="form-group">
              <label>Emri i Partisë</label>
              <input type="text" name="name" parsley-trigger="change" required="" placeholder="Emri" autocomplete="off" class="form-control" value="{{$party->name}}">
            </div>
            <div class="form-group">
              <label>Shkurtesa Partisë</label>
              <input type="text" name="acronym" parsley-trigger="change" required="" placeholder="Emri" autocomplete="off" class="form-control" value="{{$party->acronym}}">
            </div>
            <div class="form-group">
              <label>Moto e Partisë</label>
                <input type="text" name="moto" parsley-trigger="change" required="" placeholder="Moto..." autocomplete="off" class="form-control" value=" {{$party->moto}}">
            </div>
            <div class="form-group">
              <label>Numri i Partisë</label>
                <input type="number" name="number" min="0" parsley-trigger="change" required="" placeholder="1 2..." autocomplete="off" class="form-control" value="{{$party->number}}">
            </div>
            <div class="form-group">
              <label>Logo e Partisë</label>
              <input type="file" name="logo" class="form-control">
            </div>
             <div class="form-group">
                <label>Ngjyra<span class="text-danger">*</span></label>
                <select class="form-control" name="theme">
                        <option {{ $party->theme == 'red' ? 'selected' : '' }} value="red">red</option>
                        <option {{ $party->theme == 'blue' ? 'selected' : '' }} value="blue">blue</option>
                        <option {{ $party->theme == 'blueviolet' ? 'selected' : '' }} value="blueviolet">blueviolet</option>
                        <option {{ $party->theme == 'goldenrod' ? 'selected' : '' }} value="goldenrod">goldenrod</option>
                        <option {{ $party->theme == 'green' ? 'selected' : '' }} value="green">green</option>
                        <option {{ $party->theme == 'magenta' ? 'selected' : '' }} value="magenta">magenta</option>
                        <option {{ $party->theme == 'purple'  ? 'selected' : '' }} value="purple">purple</option>
                        <option {{ $party->theme == 'yellow' ? 'selected' : '' }} value="yellow">yellow</option>
                        <option {{ $party->theme == 'yellowgreen' ? 'selected' : '' }} value="yellowgreen">yellowgreen</option>
                        <option {{ $party->theme == 'aak' ? 'selected' : '' }} value="aak">aak - darkblue</option>
                        <option {{ $party->theme == 'pdk' ? 'selected' : '' }} value="pdk">pdk - lightblue</option>
                </select>
                <img src="/light/img/styleswitcher/red.png" alt="red"/>
                <img src="/light/img/styleswitcher/blue.png" alt="blue"/>
                <img src="/light/img/styleswitcher/blueviolet.png" alt="blueviolet"/>
                <img src="/light/img/styleswitcher/goldenrod.png" alt="goldenrod"/>
                <img src="/light/img/styleswitcher/green.png" alt="green"/>
                <img src="/light/img/styleswitcher/magenta.png" alt="magenta"/>
                <img src="/light/img/styleswitcher/purple.png" alt="purple"/>
                <img src="/light/img/styleswitcher/yellow.png" alt="yellow"/>
                <img src="/light/img/styleswitcher/yellowgreen.png" alt="yellowgreen"/>
            </div>
            <p class="text-right">
              <button type="submit" class="btn btn-space btn-primary">Submit</button>
            </p>
      </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_footer')
    <script src="/js/app-form-elements.js" type="text/javascript"></script>
    <script src="/js/main.js" type="text/javascript"></script>
    <script src="/lib/parsley/parsley.min.js" type="text/javascript"></script>
@endsection

@section('custom_scripts')

@endsection
