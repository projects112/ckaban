@extends('layouts.admin.admin')
  @section('pageTitle', 'Kandidatët Politikë')

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card card-table">
      <div class="card-header">Kandidatët Politikë</div>
      <div class="card-body">
        <table class="table table-striped table-hover table-fw-widget" id="table1">
          <thead>
            <tr>
              <th>Nr</th>
              <th>Logo</th>
              <th>Emri & Mbiemri </th>
              <th>Nr. i kandidatit</th>
              <th>Partia</th>
              <th>Veprime</th>
       `     </tr>
          </thead>
          <tbody>

            @foreach($candidates as $candidate)
             <tr class="odd gradeX">
              <td>{{$candidate->id}}</td>

               @if($candidate->party)
              <td><img style="width:50px;height:30px; " src="{{asset('/images/party/'.$candidate->party->logo)}}"></td>
                @else
                 <td style="color: red">null</td>
              @endif
              <td>{{$candidate->full_name}}</td>
              <td>{{$candidate->number}}</td>
              @if($candidate->party)
                 <td>{{$candidate->party->name}}</td>
              @else
                 <td style="color: red">null</td>
              @endif
              <br>

              <td><a href="{{route('admin.candidates.edit', $candidate->id)}}" class="btn mr-2 btn-info">Edito</a>
                <form action="{{route('admin.candidates.destroy', $candidate)}}" method="POST" class="w-100 d-inline">
                  @csrf
                  @method('Delete')
                <button type="submit" class="btn mr-2 btn-danger">Fshije</button>
                </form>
              </td>
            </tr>
            @endforeach


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_footer')
  <script src="/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/jszip/jszip.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/pdfmake/pdfmake.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/pdfmake/vfs_fonts.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
  <script src="/lib/datatables/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>
  <script src="/js/app-tables-datatables.js" type="text/javascript"></script>
@endsection

@section('custom_scripts')
 <script type="text/javascript">;
          $(document).ready(function(){
            //-initialize the javascript
            App.init();
            App.dataTables();
          });
        </script>
@endsection
