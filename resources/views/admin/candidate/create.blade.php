@extends('layouts.admin.admin')
@section('pageTitle', 'Shto Kandidat')

@section('custom_header')
    {{--      <meta name="csrf-token" content="{{csrf_token()}}">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <link rel="stylesheet" type="text/css" href="/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="/lib/summernote/summernote.css"/>

@endsection
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Shto Kandidat</div>
                <div class="card-body">
                    <form id="createForm" action="{{route('admin.candidates.store')}}" method="POST"
                          data-parsley-validate=""
                          novalidate="" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group no-padding">
                            <div>
                                <h3 class="wizard-title">Të dhënat personale: </h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label>Emri i Plotë<span class="text-danger">*</span></label>
                                <input type="text" id="title" name="full_name" parsley-trigger="change" required="" placeholder="Emri"
                                       autocomplete="off" class="form-control" value="{{old('full_name')}}">
                            </div>
                            <div class="col">
                                <label>Slug<span class="text-danger">*</span></label>
                                <input type="text" id="slug" name="slug" parsley-trigger="change" required=""
                                       placeholder="emri-mbiemri" autocomplete="off" class="form-control"
                                       value="{{old('slug')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Short-Bio</label>
                            <div>
                                <textarea name="short_bio" class="form-control"
                                          value="{{old('short_bio')}}" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group ">
                            <h4 class="wizard-title">Gjinia: </h4>
                            <div class="form-check pl-3 form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1"
                                       value="female" checked>
                                <label class="form-check-label" for="inlineRadio1"><h4>Femër</h4></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2"
                                       value="male">
                                <label class="form-check-label" for="inlineRadio2"><h4>Mashkull</h4></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio3"
                                       value="other">
                                <label class="form-check-label" for="inlineRadio3"><h4>Tjetër</h4></label>
                            </div>
                        </div>
                        <div class="form-group no-padding">
                            <div>
                                <h3 class="wizard-title">Të dhënat zgjedhore: </h3>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col">
                                <label>Komuna<span class="text-danger">*</span></label>
                                {{--                                <input type="text" name="municipality" parsley-trigger="change" required="" placeholder="Komuna"--}}
                                {{--                                       autocomplete="off" class="form-control" value="{{old('municipality')}}">--}}
                                <select class="form-control" name="municipality">
                                    <option selected disabled>Zgjidh Komunën...</option>
                                    @foreach($municipalities as $municipality)
                                        <option value="{{$municipality}}">{{ucfirst($municipality)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <label>Numri i kandidatit</label>
                                <input type="number" name="number" parsley-trigger="change"
                                       placeholder="Nr.." autocomplete="off" class="form-control"
                                       value="{{old('number')}}" min="0">
                            </div>
                            <div class="col">
                                <label>Partia<span class="text-danger">*</span></label>
                                <select class="form-control" name="party_id">
                                    <option selected disabled>Zgjidh partinë...</option>
                                    @foreach($parties as $party)
                                        <option value="{{$party->id}}">{{$party->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Ckaban</label>
                            <div>
                                <textarea name="ckaban" class="form-control"
                                          value="{{old('ckaban')}}" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group no-padding">
                            <div>
                                <h3 class="wizard-title">Profilet e rrjeteve sociale: </h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label>Facebook</label>
                                <input type="text" name="facebook" parsley-trigger="change"
                                       placeholder="Facebook"
                                       autocomplete="off" class="form-control" value="{{old('facebook')}}">
                            </div>
                            <div class="col">
                                <label>Instagram</label>
                                <input type="text" name="instagram" parsley-trigger="change"
                                       placeholder="Instagram" autocomplete="off" class="form-control"
                                       value="{{old('instagram')}}">
                            </div>
                            <div class="col">
                                <label>Twitter</label>
                                <input type="text" name="twitter" parsley-trigger="change"
                                       placeholder="Twitter" autocomplete="off" class="form-control"
                                       value="{{old('twitter')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label>Email</label>
                                <input type="email" name="email" parsley-trigger="change" placeholder="E-mail"
                                       autocomplete="off" class="form-control" value="{{old('email')}}">
                            </div>
                        </div>

                        <div class="form-group no-padding">
                            <div>
                                <h3 class="wizard-title">Fotot: </h3>
                            </div>
                        </div>
                        <div class="form-group">
                            <input id="inputpcimage" type="hidden" name="pcimg" class="form-control">
                        </div>

                        <div class="form-group">
                            <input id="inputphoneimage" type="hidden" name="phoneimg" class="form-control">
                        </div>
                    </form>
                    {{--Phone image crop--}}
                    <div class="container mt-5">
                        <div class="card">
                            <div class="card-header bg-primary text-center text-white">Foto e kandidatit per Telefon
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <div id="upload-demo"></div>
                                    </div>
                                    <div class="col-md-4" style="padding:5%;">
                                        <strong>Zgidh Foton:</strong>
                                        <input type="file" id="image">

                                        <button class="btn btn-success btn-block upload-image" style="margin-top:2%">
                                            Perfundo
                                        </button>
                                    </div>

                                    <div class="col-md-4">
                                        <div id="preview-crop-image"
                                             style="background:#9d9d9d;width:300px;padding:50px 50px;height:300px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{--Pc image crop--}}
                    <div class="container ">
                        <div class="card">
                            <div class="card-header bg-primary text-center text-white">Foto e kandidatit per Kompjuter
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <div id="pc-upload-demo"></div>
                                    </div>
                                    <div class="col-md-4" style="padding:5%;">
                                        <strong>Zgidh Foton:</strong>
                                        <input type="file" id="pcimage">

                                        <button class="btn btn-success btn-block upload-pc-image" style="margin-top:2%">
                                            Perfundo
                                        </button>
                                    </div>

                                    <div class="col-md-4">
                                        <div id="preview-crop-pc-image"
                                             style="background:#9d9d9d;width:270px;padding:50px 60px;height:320px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="text-right">
                        <button disabled type="submit" id="submitbtn" class="btn btn-space btn-primary">Submit
                        </button>
                    </p>


                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
          $('#title').on('input',function(e) {
            $.get('{{ route('admin.check_slug') }}',
              { 'full_name': $(this).val() },
              function( data ) {
                $('#slug').val(data.slug);
              }
            );
          });

        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,
            // enableResize: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' }
                width: 200,
                height: 200,
                type: 'square', //square
                backgroundColor: 'blue',

            },
            boundary: {
                width: 270,
                height: 270
            },
        });

        $('#image').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                resize.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
        });
        $('.upload-image').on('click', function (ev) {
            resize.croppie('result', {
                type: 'canvas',
                // size: 'viewport',
                size: {
                    width: 270,
                    height: 270,
                }
            }).then(function (img) {
                html = '<img style="max-width: 200px; max-height: 200px" src="' + img + '" />';
                $("#preview-crop-image").html(html);
                $("#inputphoneimage").val(img);
                if ($('#inputpcimage').val() != "") {
                    $("#submitbtn").prop("disabled", false);
                }
            });
        });
    </script>


    {{-- PC Upload AJAX--}}
    <script type="text/javascript">
        var resizepc = $('#pc-upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,
            viewport: {
                width: 150,
                height: 200
            },
            boundary: {
                width: 270,
                height: 270
            },
        });
        $('#pcimage').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                resizepc.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
        });
        $('.upload-pc-image').on('click', function (ev) {
            resizepc.croppie('result', {
                type: 'canvas',
                size: {
                    width: 746,
                    height: 1020
                }
            }).then(function (pcimg) {
                html = '<img style="max-width: 150px; max-height: 200px" src="' + pcimg + '" />';
                $("#preview-crop-pc-image").html(html)
                $("#inputpcimage").val(pcimg);
                if ($('#inputphoneimage').val() != "") {
                    $("#submitbtn").prop("disabled", false);
                }
            });
        });
        $("#submitbtn").on('click', function () {
            $("#createForm").submit();
        });
    </script>

@endsection

@section('custom_footer')
    <script src="/js/app-form-elements.js" type="text/javascript"></script>
    <script src="/js/main.js" type="text/javascript"></script>
    <script src="/lib/parsley/parsley.min.js" type="text/javascript"></script>
    <script src="/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="/js/app-form-elements.js" type="text/javascript"></script>
@endsection

@section('custom_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
        });
    </script>
@endsection
