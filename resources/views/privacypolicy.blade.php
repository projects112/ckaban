@extends('layouts.app')
@section('pageTitle','CookiesPrivacy')
@section('page_theme', 'blog light')

@section('theme')
 <style type="text/css">
      body {
        background-color: #eff0f0;
      }

      h2,h3,h5 {
        text-align: center;
      }
      p {
        text-align: justify;
      }
    </style>

@section('content')

<div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 style="padding-top: 30px;" >Politika e Privatësisë </h2>
        <h5>Objektivat dhe Fushëveprimi i Politikës së Privatësisë</h5>
          <p>Ckabon ofron shërbime të bazuara në Internet të cilat lehtësojnë jetën në epokën digjitale. Ju mund të kërkoni apo shfletoni informata të ndryshme dhe e gjithë kjo na bën shumë krenarë dhe të lumtur. <br>
          Kur ju përdorni shërbimet tona ju ndani disa informata me ne. Privatësia juaj është jashtëzakonisht e rëndësishme për ne. Ne përgjigje të rregullores së re për mbrojtjen e të dhënave (GDPR), që ka hyrë në fuqi në Bashkimin Evropian më 25 Maj 2018, ne kemi përditesuar Kushtet e Shërbimit dhe Politikat e Privatësisë. Megjithëse kjo rregullore kërkon zbatim vetëm për qytetarët në Bashkimin Evropian, ne kemi filluar t’i aplikojmë këto përditësime në nivel global duke përfshirë Kosovën. <br>
          Ne jemi të përkushtuar të respektojmë dhe mbrojmë privatësinë tuaj. Çfarëdo pyetje që keni në lidhje me politikat e privatësisë ju lutem mos hezitoni të na kontaktoni. <br>
          Këto politika që rregullojnë procedurat dhe masat e sigurisë për mbledhjen e të dhënave janë në përputhje me Ligjin e aplikueshëm.
          </p>

        <h3>  Mbledhja e të Dhënave Personale</h3>
        <p>
          Ne mund të mbledhim të dhënat dhe informatat tuaja personale (përfshirë edhe pajisjet e ndryshme) kur ju: <br>
          Shfrytëzoni shërbimet tona; <br>
          Dërgoni CV tuaj për regjistrim <br>
          Komunikoni me ne në lidhje me shërbimet tona, dhe; <br>
          Rastet tjera siç mund të jenë të specifikuara si më poshtë në këtë Politikë të Privatësisë.
        </p>

        <h3>Të dhënat personale që ne mund t’i mbledhim përfshijnë si vijon: </h3>
         <p> Informatat identifikuese siç janë: emri dhe mbiemri, adresa, numri i telefonit, fjalëkalimi apo e-mail adresa, kur ju regjistroni për një llogari me ne. <br>
        Në disa raste, kur ju përdorni shërbimet tona, ju mund të ofroni informata personale siç mund të jenë: mosha, gjinia, interesat, përvoja, edukimi dhe preferencat tuaja. <br>
        Informata shtesë që kërkohen apo autorizohen nga ligjet e aplikueshme për të mbledhur dhe të procesuar me qëllim identifikimin dhe verifikimin e informacionit që kemi mbledhur nga ju.</p>

        <h3>  Të dhënat automatike </h3>
        <p>Informata që lidhen me ndërveprimin tuaj me shërbimet tona; <br>
        Preferencat e juaja të marketingut, reklamave dhe komunikimit tuaj me ne. Këtë informacion ne e pranojmë nga pajisjet që ju i përdorni kur shfrytëzoni shërbimet tona, regjistroheni për një llogari te ne, përditësoni apo shtoni informata në llogarinë tuaj, participoni në diskutime apo chat-e, apo kur ju komunikoni me ne në lidhje me shërbimet tona. Ky informacion përfshin si vijon: ID-në e pajisjes tuaj apo numrin unik identifikues dhe llojin e pajisjes, sistemin operativ apo të ngjashme. <br>
        Informata mbi vendndodhjen gjeografike, nga IP adresa apo të shënimeve tjera të pajisjes. <br>
        Informatat e kompjuterit, siç mund të jenë statistikat e shikimit të faqeve nga ana e juaj, qarkullimi për apo nga sajtet e caktuara, linqeve, referimet URL, IP adresat tuaja, historinë tuaj të shfletimeve apo të dhënat tuaja të kyçjes në ueb faqet tona, apo partnereve tanë. <br>
        Termet e kërkimit apo filterët e përdorur. </p>

        <h3>Të dhënat nga burime të tjera </h3>
        <p> Ne mund të plotësojmë të dhënat personale që ne mbledhim me informacione nga palët e treta dhe të shtojmë atë në informacionin e llogarisë suaj. Për shembull, ne mund të mbledhim dhe të përdorim informacione demografike që janë publikisht në dispozicion, siç lejohet nga ligjet e aplikueshme. <br>
        Personalizimi nëpër pajisje te ndryshme. Kur ju qaseni në Ckabon nga shfletuesi apo pajisja ne e asocojmë shfletuesin me atë shfletues apo pajisje me llogarinë e juaj për qëllime te kyçjes, sigurisë dhe personalizimit.</p>

         <h3>Si i ruajmë të dhënat e mbledhura </h3>
        <p>Ne ruajmë të dhënat tuaja personale për aq kohë sa është e nevojshme dhe janë të rëndësishme për ofrimin e shërbimeve tona, në përputhje me ligjet në fuqi. Përveç kësaj, ne mund të ruajmë të dhënat tuaja personale edhe pas mbylljes së llogarisë tuaj për të parandaluar mashtrimin, për të mbledhur ndonjë borxh të papaguar, zgjidhjen e kontesteve, për të zbatuar marrëveshjen tonë për Kushtet e Përdorimit, për të ndihmuar çdo hetim, dhe të ndërmarrë veprime të tjera të lejuara ose të kërkuara me ligjet e aplikueshme. <br>
        Në rastet kur nuk është më e nevojshme për ne për të ruajtur të dhënat tuaja personale apo kur shuhet qëllimi për të cilin janë mbledhur apo ruajtur të dhënat tuaja personale, ne bëjmë shkatërrimin e tyre në mënyrë të sigurtë në përputhje me ligjet nacionale për privatësinë dhe mbrojtjen e të dhënave personale dhe politikave tona të brendshme. </p>

        <h3>Ligji dhe interesi publik</h3>
        <p>Duke mos ju përmbajte asgjëje që është në të kundërt të kësaj politike të privatësisë apo kontrollit te cilat përndryshe mund t’ju kemi ofruar juve, ne ruajmë, përdorim, apo shpalosim te dhënat e juaja personale nëse besojmë që janë arsyeshmërisht të domosdoshme për të qenë në përputhje me ligj, procese rregullative, apo kërkesa qeveritare; për të mbrojtur sigurinë e cilit çdo person, për të mbrojtur sigurinë dhe integritetin e platformës tonë, përfshi ndihmën që të parandalojmë spam, abuzim, apo veprime të pa lejuara në shërbime tona, apo për të shpjeguar pse i kemi larguar përmbajtjet e llogarive prej shërbimeve tona; për të adresuar mashtrim, qështje të sigurisë apo qështje teknike; apo për të mbrojtur të drejtat tona apo pronën e atyre që përdorin shërbimet tona. Gjithsesi, asgjë në këtë politikë të privatësisë nuk ka për qëllim të ju kufizoje juve për mbrojtje ligjore apo kundërshtim që ju mund të keni ndaj palëve të treta, përfshi kërkesat e qeverive për shpalosje të të dhënave personale. </p>


        <h3>Kundërshtimi, kufizimi apo tërheqja e pëlqimit</h3>
        <p> Kur ju jeni të kyçur në llogari të Ckabon, ju mund të menaxhoni konfigurimet e privatësisë dhe funksionet e llogarisë në SSO në çdo kohë. Gjithsesi nëse ju bëni revokimin e pëlqimit tuaj për mbledhje, përdorim, procesim, transfer apo shpalosje të të dhënave tuaja personale për qëllimet e specifikuara në këtë Politikë të Privatësisë, ju mund të mos keni qasje në të gjitha shërbimet tona, dhe ne mund të mos jemi në gjendje të ju ofrojmë juve të gjitha shërbimet dhe mbështetjen që ofrohet për përdoruesit e tjerë.</p>

        <h3> Preferencat e komunikimit </h3>
        <p> Ju keni mundësi të zgjedhni se si ne mund të përdorim të dhënat tuaja personale për të komunikuar me ju, për t’ju dërguar njoftime informuese dhe se si ne mund të ju dërgojmë reklama të personalizuara dhe relevante.</p>  

        <h3>Sugjerimet dhe Ankesat</h3>
        <p>Nëse ju keni ndonjë pyetje, sugjerim apo ankesë për këtë Politikë të Privatësisë, apo në lidhje me të drejtën tuaj të privatësisë dhe mbrojtjen e të dhënave tuaja personale ju mund të na kontaktoni përmes e-mailit: <a href="mailto: info@ckabon.net"> info@ckabon.net</a> </p>
        <h3>Ndryshimet në këtë Politikë të Privatësisë</h3>
        <p>Ne mund ta revidojmë kohë pas kohe këtë politikë të privatësisë. Versioni më i përditësuar do të rregullojë procesimin tonë të informative tuaja personale dhe gjithmonë do të jetë në  . Nëse bëjme ndryshime në këtë politikë, që sipas vlerësimit tonë është me peshë do të ju njoftojmë përmes faqes tonë ne facebook ose e-mail. Cila do mënyrë që përdoret për njoftim është zyrtare. Duke vazhduar te çaseni apo përdorni shërbimet pasi që këto ndryshime të hyjnë në fuqi ju pajtoheni me përditësimet.</p>

      </div>
      <div class="col-md-3"></div>
      <a type="button" style="margin-bottom: 20px;" class="btn btn-success col-md-6" href="">Vazhdo me faqen kryesore</a>
      <br>
    </div>
  </div>


@endsection

@section('custom_footer')

@endsection