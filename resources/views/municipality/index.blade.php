@extends('layouts.app')
@section('pageTitle', 'Komunat')
@section('theme', 'green')
@section('page_theme', 'blog light')

@section('custom_header')
    <meta property="og:image" content="{{asset('light\img\og.jpg')}}" />
    <style>
.contactform input[type=text]{
  border: 1px solid #111;
  width: 100%;
  background: #252525;
  color: #fff;
  padding: 15px 26px;
  border-radius: 30px;
  outline: none !important;
  transition: .3s;
}

    </style>
@endsection
@section('content')
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1> <span>Komunat</span></h1>
    <span class="title-bg">ckabon</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
{{--            <form method="GET" action="#">--}}
                <div class="contactform" style="margin: 0 auto">
                        <div class="row">

                        <div class="col-12 col-md-8 m-auto">
                            <input type="text" name="name" class="submit" placeholder="Search">

                        </div>
{{--                        <button type="submit" class="submit button d-none d-lg-block">--}}
{{--                            <span class="button-text">Kërko</span>--}}
{{--                            <span class="button-icon fa fa-send"></span>--}}
{{--                        </button>--}}
                        </div>
                </div>
{{--            </form>--}}
        <!-- Articles Starts -->
        <div class="row" style="margin-top: 25px;">
            <!-- Article Starts -->
            @foreach($municipalities as $municipality)
            <div class="col-12 cities col-md-6 col-lg-6 col-xl-3 mb-30">
                <article class="post-container">
                    <div class="post-thumb">
                        <a href="blog-post.html" class="d-block position-relative overflow-hidden">
                        </a>
                    </div>
                    <div class="post-content">
                        <div class="entry-header">
                            <h3 style="text-align: center"><a href="{{route('munipicality.show', $municipality)}}">{{ucfirst($municipality)}}{{ $candidates->where('municipality', $municipality)->count() > 0 ? '('.$candidates->where('municipality', $municipality)->count().')' : ''}}</a></h3>
                        </div>
                    </div>
                </article>
            </div>
            @endforeach
            <!-- Article Ends -->

{{--            <!-- Pagination Starts -->--}}
{{--            <div class="col-12 mt-4">--}}
{{--                <nav aria-label="Page navigation example">--}}
{{--                    <ul class="pagination justify-content-center mb-0">--}}
{{--                        <li class="page-item"><a class="page-link" href="#">1</a></li>--}}
{{--                        <li class="page-item active"><a class="page-link" href="#">2</a></li>--}}
{{--                        <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
{{--                        <li class="page-item"><a class="page-link" href="#">4</a></li>--}}
{{--                    </ul>--}}
{{--                </nav>--}}
{{--            </div>--}}
{{--            <!-- Pagination Ends -->--}}
        </div>
        <!-- Articles Ends -->
    </div>

</section>

<footer class="footer" style="
  pflex-grow: 0;
  flex-shrink: 0;
  flex-basis: auto;
  padding: 5px;
  text-align: center;">Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/en">TAG Digitals</a>' © <script>document.write(new Date().getFullYear())</script>
</footer>
@endsection

@section('custom_footer')
    <script src="filter.js"></script>
@endsection

