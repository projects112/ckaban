@extends('layouts.app')
@section('pageTitle', 'Subjektet Politike')
@section('theme', 'green')
@section('page_theme', 'portfolio light')

@section('custom_header')
    <meta property="og:image" content="{{asset('light\img\og.jpg')}}" />
@endsection
@section('content')
<!-- Header Ends -->
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1><span>Subjektet Politike</span></h1>
    <span class="title-bg">Ckabon</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content text-center revealator-slideup revealator-once revealator-delay1">
    <div  id="grid-gallery" class="container grid-gallery">
        <!-- Portfolio Grid Starts -->
        <section class="grid-wrap">
            <ul class="row grid">
                <!-- Portfolio Item Starts -->
               @foreach($parties as $party)
                <li onmouseleave="revertTheme()">
                    <figure onclick="redirect({{$party->id}})">
                        <img onmouseover="changetheme('{{$party->theme}}')"  src="/images/party/{{$party->logo}}" alt="{{$party->name}}" />
                        <div><span>{{$party->moto}}</span></div>
                    </figure>
                    <p>{{$party->name}}</p>
                </li>
                @endforeach
                <!-- Portfolio Item Ends -->
            </ul>
        </section>
        <!-- Portfolio Grid Ends -->
        <!-- Portfolio Details Starts -->
        <section class="slideshow">
            <ul>
                @foreach($parties as $party)
                <!-- Portfolio Item Detail Starts -->
                <li>
                    <figure>
                        <!-- Main Project Content Starts -->
                        <div class="videocontainer" >
                            <img src="/images/party/{{$party->logo}}" alt="{{$party->name}}" />
                        </div>
                        <!-- Main Project Content Ends -->
                        <!-- Project Details Starts -->
                        <figcaption>
                            <h3>{{$party->name}}</h3>
                            <div class="row open-sans-font">

                                <div class="col-6 mb-2">
                                    <i class="fa fa-user-o pr-2"></i><span class="project-label ft-wt-600 uppercase"><a href="{{url("partite/$party->id")}}" > Shiko Kandidatët </a></span>: <span class="ft-wt-600 uppercase">{{count($party->candidates)}}</span>
                                </div>
                                <div class="col-6 mb-2">
                                    <i class="fa fa-commenting-o"></i><span class="project-label"> Moto:  </span> <span class="ft-wt-600 uppercase">{{$party->moto}}</span>
                                </div>

                            </div>
                        </figcaption>
                        <!-- Project Details Ends -->
                    </figure>
                </li>
                <!-- Portfolio Item Detail Ends -->
                    @endforeach
            </ul>
            <!-- Portfolio Navigation Starts -->
            <nav>
                <span class="icon nav-prev"><img src="/light/img/projects/navigation/left-arrow-light.png" alt="previous"></span>
                <span class="icon nav-next"><img src="/light/img/projects/navigation/right-arrow-light.png" alt="next"></span>
                <span class="nav-close"><img src="/light/img/projects/navigation/close-button-light.png" alt="close"> </span>
            </nav>
            <!-- Portfolio Navigation Ends -->
        </section>
    </div>
</section>
<footer class="footer " style="
  pflex-grow: 0;
  flex-shrink: 0;
  flex-basis: auto;
  padding: 5px;
  text-align: center;">Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/en">TAG Digitals</a>' © <script>document.write(new Date().getFullYear())</script>
</footer>
<!-- Main Content Ends -->
@endsection

@section('custom_footer')
    <script>

        function redirect(party){
            document.location.href = `/partite/${party}`;
        }
        function changetheme(theme){
            if (getActiveStyleSheet() == theme){
            }
            setActiveStyleSheet(theme);
        }
        function revertTheme(){
            setActiveStyleSheet('green');
        }
    </script>
@endsection
