@extends('layouts.app')
@section('pageTitle', 'Kandidatet - '.ucfirst($municipality))
@section('page_theme', 'blog light')

@section('theme', 'green')

@section('content')
    <!-- Page Title Starts -->
    <section
        class="title-section text-left text-sm-center revealator-slideup revealatocandidate->r-once revealator-delay1">
        <h1><span>{{ucfirst($municipality)}}</span></h1>
        <span class="title-bg">Ckabon</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <!-- Articles Starts -->
            <div class="row">
                <!-- Article Starts -->
                <?php $detect = new Mobile_Detect();?>
                @forelse($candidates as $candidate)
                    @if(!$detect->isMobile())
                        <div class="col-12 col-md-3 col-lg-3 col-xl-3 mb-30">
                            <article class="post-container">
                                <div class="post-thumb">
                                    <a href="{{url("/$candidate->slug")}}"
                                       class="d-block position-relative overflow-hidden">
                                        <img src="{{asset('images/phone/'.$candidate->phoneimage)}}" width="255"
                                             height="230" class="img-fluid" alt="{{$candidate->full_name}}">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <div class="entry-header">
                                        <h3 style="text-transform: capitalize"><a
                                                href="{{url("/$candidate->slug")}}">{{$candidate->full_name}}<span
                                                    style="color: #72b626"> {{$candidate->number}}</span></a></h3>

                                        <h6 style="text-transform: uppercase;">
                                            {{ $candidate->party ? $candidate->party->acronym. ' -' : ''}} <span
                                                style="text-transform: capitalize">{{$candidate->municipality}}</span>
                                        </h6>
                                    </div>
                                </div>
                            </article>
                            @else
                                <div style="margin: 0 auto; padding-top: 15px;">
                                    <article class="post-container" style="width: 250px;">
                                        <div class="post-thumb">
                                            <a href="{{url("/$candidate->slug")}}"
                                               class="d-block position-relative overflow-hidden">
                                                <img src="{{asset('images/phone/'.$candidate->phoneimage)}}" width="255"
                                                     height="230" class="img-fluid" alt="{{$candidate->full_name}}">
                                            </a>
                                        </div>
                                        <div class="post-content">
                                            <div class="entry-header">
                                                <h3 style="text-transform: capitalize"><a
                                                        href="{{url("/$candidate->slug")}}">{{$candidate->full_name}}
                                                        <span style="color: #72b626"> {{$candidate->number}}</span></a>
                                                </h3>

                                                <h6 style="text-transform: uppercase;">
                                                    {{ $candidate->party ? $candidate->party->acronym. ' -' : ''}} <span
                                                        style="text-transform: capitalize">{{$candidate->municipality}}</span>
                                                </h6>
                                            </div>
                                        </div>
                                    </article>
                                    @endif
                                </div>
                            @empty
                                    <h1 style="margin: 0 auto;">Kandidatët prej komunës: {{ucfirst($municipality)}} nuk janë gjetur.</h1>
                            @endforelse
                            <!-- Article Ends -->

                                <!-- Pagination Starts -->
                            {{-- <div class="col-12 mt-4">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center mb-0">
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    </ul>
                                </nav>
                            </div> --}}


                            <!-- Pagination Ends -->
                        </div>
                        <!-- Articles Ends -->
            </div>
        </div>
    </section>


    {{-- @if($detect->isMobile())--}}
    <footer class="footer " style="
  pflex-grow: 0;
  flex-shrink: 0;
  flex-basis: auto;
  padding: 5px;
  text-align: center;">Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/en">TAG
            Digitals</a>' ©
        <script>document.write(new Date().getFullYear())</script>
    </footer>
@endsection

@section('custom_footer')

@endsection

