@extends('layouts.app')
@section('pageTitle', 'Kandidatet - '.ucfirst($municipality))
@section('page_theme', 'blog light')

{{--set page theme color if isset else set to green--}}
@section('theme', $party->theme ? $party->theme : 'green' )
@section('custom_header')
    <style>
        .contactform input[type=text] {
            border: 1px solid #111;
            width: 100%;
            background: #252525;
            color: #fff;
            padding: 15px 26px;
            border-radius: 30px;
            outline: none !important;
            transition: .3s;
        }
        .dropdown-item {
            border-radius: 25px;
        }
    </style>
@endsection
@section('content')
    <!-- Page Title Starts -->
    <section
        class="title-section text-left text-sm-center revealator-slideup revealatocandidate->r-once revealator-delay1">
        <h1>{{$party->acronym}} - <span>{{ucfirst($municipality)}}</span></h1>
        <span class="title-bg">Ckabon</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    @if($candidates->count() != 0)
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <div class="contactform" style="margin: 0 auto">
                <div class="row">
                    <div class="col-12 col-md-8 m-auto">
                        <input type="text" name="search" class="submit" autocomplete="off" placeholder="Search">

                        <div class="dropdown ">
                            <div class="dropdown-menu col-md-12" style="border-radius:25px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
            <!-- Articles Starts -->
            <div class="row mt-4">
                <!-- Article Starts -->
                <?php $detect = new Mobile_Detect();?>
                @forelse($candidates as $candidate)
                    @if(!$detect->isMobile())
                        <div class="col-12 col-md-3 col-lg-3 col-xl-3 mb-30">
                            <article class="post-container">
                                <div class="post-thumb">
                                    <a href="{{url("/$candidate->slug")}}"
                                       class="d-block position-relative overflow-hidden">
                                        <img src="{{asset('images/phone/'.$candidate->phoneimage)}}" width="255"
                                             height="230" class="img-fluid" alt="{{$candidate->full_name}}">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <div class="entry-header">
                                        <h3 style="text-transform: capitalize"><a
                                                href="{{url("/$candidate->slug")}}">{{$candidate->full_name}} {{$candidate->number}}</a></h3>

                                        <h6 style="text-transform: uppercase;">
                                            {{ $candidate->party ? $candidate->party->acronym. ' -' : ''}} <span
                                                style="text-transform: capitalize">{{$candidate->municipality}}</span>
                                        </h6>
                                    </div>
                                </div>
                            </article>
                            @else
                                <div style="margin: 0 auto; padding-top: 15px;">
                                    <article class="post-container" style="width: 250px;">
                                        <div class="post-thumb">
                                            <a href="{{url("/$candidate->slug")}}"
                                               class="d-block position-relative overflow-hidden">
                                                <img src="{{asset('images/phone/'.$candidate->phoneimage)}}" width="255"
                                                     height="230" class="img-fluid" alt="{{$candidate->full_name}}">
                                            </a>
                                        </div>
                                        <div class="post-content">
                                            <div class="entry-header">
                                                <h3 style="text-transform: capitalize"><a
                                                        href="{{url("/$candidate->slug")}}">{{$candidate->full_name}} {{$candidate->number}}</span></a>
                                                </h3>

                                                <h6 style="text-transform: uppercase;">
                                                    {{ $candidate->party ? $candidate->party->acronym. ' -' : ''}} <span
                                                        style="text-transform: capitalize">{{$candidate->municipality}}</span>
                                                </h6>
                                            </div>
                                        </div>
                                    </article>
                                    @endif
                                </div>
                            @empty
                                <div class="col-md-12 col-sm-6 mt-5">
                                    <h1 style="text-align: center;">Kandidatët e  {{ $party->acronym == 'Të Pavarur' ? 'Pavarur' : $party->acronym.'-së'  }} për komunën {{ucfirst($municipality)}} nuk janë gjetur!</h1>
                                </div>
                            @endforelse
                        <!-- Articles Ends -->
                            <!-- Pagination Starts -->
                                        <div class="col-12 mt-4">
                                            <nav aria-label="Page navigation example">
                                                <ul class="pagination justify-content-center mb-0">
                                                {{$candidates->links()}}
                                                </ul>
                                            </nav>
                                        </div>
                                <!-- Pagination Ends -->
                        </div>
            </div>
        </div>
    </section>


    {{-- @if($detect->isMobile())--}}
    <footer class="footer " style="
  pflex-grow: 0;
  flex-shrink: 0;
  flex-basis: auto;
  padding: 5px;
  text-align: center;">Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/en">TAG
            Digitals</a>' ©
        <script>document.write(new Date().getFullYear())</script>
    </footer>
@endsection

@section('custom_footer')
<script>
        const dropdown = document.querySelector('.dropdown-menu');


        const search = document.querySelector('.submit');
        search.addEventListener('keyup', e => {
            const query = search.value.trim();
            if (query !== ''){

            url = `/search/{{$municipality}}/{{$party->acronym}}/${query}/`;
            console.log(url)
            $.get(url,
                function (data) {
                    if (query != '') {
                        if (data['count'] !== 0) {
                            dropdown.style.display = 'block';
                        } else {
                            dropdown.style.display = 'none';
                        }
                        dropdown.innerHTML = data[0];
                    }else {
                        dropdown.style.display = 'none';
                    }
                }
            );
            }else{
                dropdown.innerHTML = "";
                dropdown.style.display = 'none';
            }

        });
    </script>
@endsection

