@extends('layouts.app')
@section('pageTitle', 'Kontakti')
@section('page_theme', 'contact light')
@section('theme', 'green')
@section('content')
    <!-- Header Ends -->
    <!-- Page Title Starts -->


    <section
        class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1 no-transform revealator-within">
        <h1><span>Kontakti</span></h1>
        <span class="title-bg">Ckabon</span>
    </section>
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <div class="row">
                <!-- Left Side Starts -->
                <div class="col-12 col-lg-4">

                    <p class="open-sans-font custom-span-contact position-relative">

                        <span class="d-block">Adresa: </span><a style="color: black;" target="_blank"
                                                                href="https://www.google.com/maps/place/TAG+Digitals/@42.6608886,21.1588364,17z/data=!3m1!4b1!4m5!3m4!1s0x13549f2fe41e9267:0x6a90698c02966ffa!8m2!3d42.6608886!4d21.1610304">
                            Bulevardi Nënë Tereza, Prishtinë 10000</a>
                        <br><a style="color: black; line-height: 35px;" target="_blank"
                               href="https://www.google.com/maps/place/TAG+Digitals/@42.3710535,21.1477077,15z/data=!4m2!3m1!1s0x0:0xec2591bf5dc64f3a?sa=X&ved=2ahUKEwjL6tuzieLxAhUr_7sIHbKMArQQ_BIwE3oECEYQBQ">
                            Ramadan Rexhepi, Ferizaj 70000</a>
                    </p>
                    <p class="open-sans-font custom-span-contact position-relative">

                        <span class="d-block">Emaili: </span><a style="color: black;"
                                                                href="mailto:info@ckabon.net">info@ckabon.net</a>
                    </p>
                    <p class="open-sans-font custom-span-contact position-relative">

                        <span class="d-block">Telefoni: </span><a style="color: black;" href="tel:+38348255461"> +383 48
                            255 461</a>
                    </p>
                </div>
                <!-- Left Side Ends -->
                <!-- Contact Form Starts -->
                <div class="col-12 col-lg-8">
                    <form method="POST" action="{{route('contact_us')}}">
                        @csrf

                        <div class="contactform">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <input type="text" name="name" placeholder="EMRI JUAJ">
                                </div>
                                <div class="col-12 col-md-6">
                                    <input type="email" name="email" placeholder="EMAIL-I JUAJ">
                                </div>

                                <div class="col-12">
                                    <textarea name="message" placeholder="Mesazhi"></textarea>
                                    <button type="submit" class="button">
                                        <span class="button-text">Dërgo Mesazh</span>
                                        <span class="button-icon fa fa-send"></span>
                                    </button>
                                </div>
                                @if (session('error'))
                                    <div class="col-12 form-message" style="background: #dc3545; display: block;
                                                                                color: #fff;
                                                                                /*height: 46px;*/
                                                                                line-height: 46px;
                                                                                border-radius: 30px;">
                                <span
                                    class="output_message text-center font-weight-600 text-uppercase">{{session('error')}}</span>
                                    </div>
                            </div>
                            @endif
                            @if (session('status'))
                                <div class="col-12 form-message" style="background: #72b626; display: block;
                                                                                color: #fff;
                                                                                height: 46px;
                                                                                line-height: 46px;
                                                                                border-radius: 30px;">
                                <span
                                    class="output_message text-center font-weight-600 text-uppercase">{{session('status')}}</span>
                                </div>
                        </div>
                        @endif
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="col-12 form-message" style="background: #dc3545; display: block;
                                                                            color: #fff;
                                                                            height: 46px;
                                                                            line-height: 46px;
                                                                            border-radius: 30px;
                                                                            margin: 2px 0;">
                            <span class="output_message text-center font-weight-600 text-uppercase">
                                        <ul>
                                                <li>{{ $error }}</li>
                                        </ul>
                            </span>
                                </div>
                    @endforeach
                </div>
                @endif

                </form>
            </div>


            {{--        <div class="col-12 col-lg-12 mt-5" style="margin: 0 auto;">--}}
            {{--                <div class="mapouter">--}}
            {{--                    <div class="gmap_canvas">--}}
            {{--                        <iframe width="700" height="350" id="gmap_canvas"--}}
            {{--                                src="https://maps.google.com/maps?q=TAG%20Digitals%20Prishtine&t=&z=13&ie=UTF8&iwloc=&output=embed"--}}
            {{--                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>--}}
            {{--                        <a href="https://soap2day-to.com">soap2day</a><br>--}}
            {{--                        <style>.mapouter {--}}
            {{--                                position: relative;--}}
            {{--                                text-align: right;--}}
            {{--                                height: 350px;--}}
            {{--                                width: 700px;--}}
            {{--                            }</style>--}}
            {{--                        <a href="https://www.embedgooglemap.net">google map for website</a>--}}
            {{--                        <style>.gmap_canvas {--}}
            {{--                                overflow: hidden;--}}
            {{--                                background: none !important;--}}
            {{--                                height: 350px;--}}
            {{--                                width: 700px;--}}
            {{--                            }</style>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}

        </div>
    </section>
     <div class="container mt-3" style="margin: 0 auto;">
                <div>
                    <div>
                        <iframe width="600" height="500" id="gmap_canvas"
                                src="https://maps.google.com/maps?q=TAG%20Digitals%20Prishtine&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                                style="width: 100% !important; border-radius: 30px"></iframe>
                        <a href="https://www.embedgooglemap.net/blog/nordvpn-coupon-code/"></a></div>
                    <style>.mapouter {
                            position: relative;
                            text-align: right;
                            height: 500px;
                            width: 600px;
                        }
                        .gmap_canvas {
                            overflow: hidden;
                            background: none !important;
                            height: 500px;
                            width: 600px;
                        }</style>
                </div>

            </div>
{{--    <?php $detect = new Mobile_Detect();?>--}}
{{--    @if($detect->isMobile())--}}
        <footer class="footer " style="
  pflex-grow: 0;
  flex-shrink: 0;
  flex-basis: auto;
  padding: 5px;
  text-align: center;">Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/en">TAG
                Digitals</a>' ©
            <script>document.write(new Date().getFullYear())</script>
        </footer>
{{--    @else--}}
{{--        <footer class="footer mt-auto py-3" style=" background: transparent;--}}
{{--                                            hight: 60px;--}}
{{--                                            width: 100%;--}}
{{--                                            margin: 0 auto;--}}
{{--                                            position: absolute;--}}
{{--                                            bottom: 0;--}}
{{--                                            clear: both;">--}}
{{--            <div class="container text-center">--}}
{{--    <span>--}}
{{--    Powered by '<a class="footer-color" target="_blank" href="http://tagdigitals.com/">TAG Digitals</a>' © <script>document.write(new Date().getFullYear())</script></span>--}}
{{--            </div>--}}
{{--        </footer>--}}
{{--    @endif--}}
@endsection

@section('custom_footer')


@endsection
